@extends('layouts.app')

@section('content')

<div class="container content">
      <div class="col-xs-12 col-sm-12">

          <form action="login"  method="post" class='log'>
              {{ csrf_field() }}

            <fieldset id='fieldlog'>

            <h2 class="center_text green_bg_color padding_10">Login Here</h2>

              <p class="padding_bottom_10">
                  <input id="email" class="form-control" type="text" name="email"  placeholder="Email" value="" /><br />
              <span class="error_message1"></span></p>


              <p class="padding_bottom_10">
              <input id="password" class="form-control" type="password" name="password" placeholder="Password" /><br />
              <span class="error_message1"></span></p>

              <a class="btn btn-link" href="{{ url('/password/reset') }}">
                  Forgot Your Password?
              </a>

              <p><input class=" button btn btn-primary" type="submit" value="Login" /></p>

             </fieldset>
        </form>
  </div>
</div>

@endsection
