@extends('layouts.app')

@section('content')

<div class="container">
    <h1 class="margin_left_10">Register</h1>
    <div class="row padding_bottom_10">
        <form id="add_customer" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
        <div class="col-xs-12 col-sm-6 ">

            <p> <span class="green_tag margin_left_10">Personal Details</span></p>

            <div class="row">

             <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="first_name" class="col-md-4 select-label">First Name</label>

                <div class="col-md-6">
                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>

                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

            <div class="row">

            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label for="last_name" class="col-md-4 select-label">Last Name</label>

                <div class="col-md-6">
                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

            <div class="row">
             <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone" class="col-md-4 select-label">Phone</label>

                <div class="col-md-6">
                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>

                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

            <div class="row">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 select-label">E-Mail Address</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

            <div class="row">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 select-label">Password</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>
            <div class="row">

                <div class="form-group">
                <label for="password-confirm" class="col-md-4 select-label">Confirm Password</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
                </div>
            </div>
            </div>

        <div class="col-xs-12 col-sm-6">

            <p> <span class="green_tag margin_left_10">Personal Details</span></p>

            <div class="row">
            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="address" class="col-md-4 select-label">Address</label>

                <div class="col-md-6">
                    <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>

                    @if ($errors->has('address'))
                        <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

            <div class="row">
            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                <label for="city" class="col-md-4 select-label">City</label>

                <div class="col-md-6">
                    <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required>

                    @if ($errors->has('city'))
                        <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

            <div class="row">
            <div class="form-group {{ $errors->has('postal_code') ? ' has-error' : '' }}">
                <label for="postal_code" class="col-md-4 select-label">Postal Code</label>

                <div class="col-md-6">
                    <input id="postal_code" type="text" class="form-control" name="postal_code" value="{{ old('postal_code') }}" required>

                    @if ($errors->has('postal_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('postal_code') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

            <div class="row">
            <div class="form-group {{ $errors->has('province') ? ' has-error' : '' }}">
                <label for="province" class="col-md-4 select-label">Province</label>

                <div class="col-md-6">
                    <input id="province" type="text" class="form-control" name="province" value="{{ old('province') }}" required>

                    @if ($errors->has('province'))
                        <span class="help-block">
                            <strong>{{ $errors->first('province') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

            <div class="row">
            <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                <label for="country" class="col-md-4 select-label">Country</label>

                <div class="col-md-6">
                    <input id="country" type="text" class="form-control" name="country" value="{{ old('country') }}" required>

                    @if ($errors->has('country'))
                        <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div>

        </div>

            <div class="form-group">
                <div class="col-xs-12">
                    <button type="submit" class="btn margin_left_10 margin_top_10 btn-primary">
                        Register
                    </button>

                </div>
            </div>
            </div>
        </form>

    </div>

@endsection
