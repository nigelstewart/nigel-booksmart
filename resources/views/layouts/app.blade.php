<!DOCTYPE html>

<html>
    <head>
        <title>Authentication</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <script src="lib/jquery/jquery-3.1.1.js"></script>
        <script src="lib/jquery/jquery-migrate-1.2.1.js"></script>
        <script src="lib/slick/slick.min.js"></script>

        <link rel="stylesheet" type="text/css" href="lib/slick/slick.css" />
        <link rel="stylesheet" type="text/css" href="lib/slick/slick-theme.css"/>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
        <script src="https://use.fontawesome.com/c001f6b9dc.js"></script>

        <link rel="icon" type="image/favicon-icon" href="images/favicon.ico" />

        <link rel="stylesheet" href="css/style.css" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Scripts -->
            <script>
                window.Laravel = <?php echo json_encode([
                    'csrfToken' => csrf_token(),
                ]); ?>
            </script>

    </head>
    <body>

    <header>
        <div class="container-fluid">
            <div class="row">

                <div class="hidden-xs col-sm-6">
                    <p class="tagline_style">Great prices on new and used books</p>
                </div>

                <div class="col-xs-12 col-sm-6">
                    @if ( Auth::guest() )
                    <ul class="util_nav">
                        <li class="cart_left_padding"><a href="#"><img src="images/cart.png" alt="cart"></a> Cart 0</li>
                        <li><a href="/login">Sign In</a></li>
                        <li><a href="/register">Sign Up</a> | </li>
                        <li><a href="/about">About Us</a> | </li>
                    </ul>
                    @else
                    <ul class="util_nav">
                        <li>
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                &nbsp;Log Out
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li>Hello, {{ ucwords(Auth::user()->first_name) }} | </li>
                    </ul>
                    @endif
                </div><!-- /col-xs-12 col-sm-6 -->
            </div><!-- /row-->

            <div class='row'>
                <div id="logo" class='responsive col-xs-12 col-sm-3'>
                    <a href="/"><img src='/images/logo.png' alt='books mart logo' /></a>
                </div><!-- /logo -->
                <div class="hidden-xs col-sm-6">

                </div>
                <div id="search" class="hidden-xs col-sm-3">
                    <div id="search_box">
                        <form action="search.php" method="get">
                            <input type="text" name="search" placeholder="Books..."/>
                            <a href="#">SEARCH</a>
                        </form>
                    </div><!-- /search-->
                </div><!-- col-cs-12 col-sm-5-->
            </div><!-- /row-->
        </div>
    </header>

    <nav class="navbar navbar-custom">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Catalogue <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Horror</a></li>
                        <li><a href="#">Romance</a></li>
                        <li><a href="#">Fiction</a></li>
                        <li><a href="#">Comedy</a></li>
                        <li><a href="#">Fantasy</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Featured</a></li>
                    <li><a href="#">Kids</a></li>
                    <li><a href="#">Sale</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <!-- THE PAGE CONTENT -->
    @yield('content')

    <footer id="footer">

        <div class="container-fluid">

            <div class="row">

                <div class="hidden-xs col-sm-3">
                    <img class="footer_logo" src="images/footer_logo.png" alt="footer logo" />
                </div>

                <div class="hidden-xs col-sm-6">
                    <ul class="footer_nav">
                        <li><a href="/">Home</a> | </li>
                        <li><a href="">Featured</a> | </li>
                        <li><a href="">Kids</a> | </li>
                        <li><a href="">Sale</a> | </li>
                        <li><a href="/contact">Contact Us</a></li>
                    </ul>
                </div>

                <div id="footer-contact" class="col-xs-12 col-sm-3">

                    <div class="location">
                        <i class="fa fa-2x fa-map-marker" aria-hidden="true" style="font-size:30px;color:#fff;"></i>
                        <span>Winnipeg, MB</span>
                    </div>

                    <div class="phone">
                        <i class="fa fa-2x fa-phone" aria-hidden="true"></i>
                        <span>+1 204-000-0000</span>
                    </div>

                    <div class="mail">
                        <i class="fa fa-lg fa-envelope" aria-hidden="true"></i>
                        <span>dream@solutions.com</span>
                    </div>

                </div>
            </div>
        </div>
    </footer>

</body>
</html>
