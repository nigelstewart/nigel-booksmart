@extends('admin/layouts/crud')

@section('body')
<body ng-app="crudApp" ng-controller="aggregatesController">
@stop

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="ag-header">Items in Database</h1>
            <canvas class="col-xs-12" id="countChart" width="400" height="200"></canvas>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-6">
            <h1 class="ag-header">Books Prices</h1>
            <canvas class="col-xs-12" id="bookPricesChart" width="400" height="200"></canvas>
        </div>

        <div class="col-xs-6">
            <h1 class="ag-header">Orders Totals</h1>
            <canvas class="col-xs-12" id="ordersTotalsChart" width="400" height="200"></canvas>
        </div>
    </div>
</div>

<script src="../../controllers/aggregates.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js"></script>
<script>

</script>

</body>
</html>
@stop
