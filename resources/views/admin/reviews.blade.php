@extends('admin/layouts/crud')

@section('body')
<body ng-app="crudApp" ng-controller="reviewsController">
@stop

@section('content')

<div class="content container-fluid">

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h1><?= $title ?></h1>
        </div>
        <div class="panel-content">
            <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-search"></i>
                <input type="text" id="search-bar" class="form-control" placeholder="Search" ng-model="searchTable"/>
            </div>
        </div>

        <!-- Table -->
        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Book</th>
                <th>User</th>
                <th>Review</th>
                <th>Stars</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tr ng-repeat="review in reviews | filter:searchTable">
                <td>@{{ review.id }}</td>
                <td>@{{ review.book_title }}</td>
                <td>@{{ review.email }}</td>
                <td>@{{ review.message }}</td>
                <td>@{{ review.stars }}</td>
                <td><span class="glyphicon glyphicon-pencil" ng-click="toggle('edit', review.id)"></span></td>
                <td><span class="glyphicon glyphicon-remove" ng-click="confirmDelete(review.id)"></span></td>
            </tr>
        </table>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="reviewModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@{{ modal_title }}</h4>
            </div>
            <div class="modal-body">

                <div class="timestamps">
                    <p>Created at: <span class="created-at">@{{ review.created_at }}</span></p>
                    <p>Updated at: <span class="updated-at">@{{ review.updated_at }}</span></p>
                </div>

                <div class="input-group">
                    <span class="input-group-addon text-right">Book: @{{ review.book_title }}</span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon text-right">User: @{{ review.email }}</span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon text-right">Stars: @{{ review.stars }}</span>
                </div>

                <div class="input-group review-group">
                    <span class="input-group-addon" id="message">Review</span>
                    <textarea class="form-control" aria-describedby="message" ng-model="review.message"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" ng-click="save(modalstate, id)">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="../../controllers/reviews.js"></script>
</body>
</html>
@stop
