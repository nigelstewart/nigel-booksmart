@extends('admin/layouts/crud')

@section('body')
<body ng-app="crudApp" ng-controller="usersController">
@stop

@section('content')
    <div class="content container-fluid">

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <h1>Users</h1>
                <p class="add-new" ng-click="toggle('add', 0)">Add <span class="glyphicon glyphicon-plus"></span></p>
            </div>
            <div class="panel-content">
                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-search"></i>
                    <input type="text" id="search-bar" class="form-control" placeholder="Search" ng-model="searchTable"/>
                </div>
            </div>

            <!-- Table -->
            <table class="table table-striped">
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <tr ng-repeat="user in users | filter:searchTable">
                    <td>@{{ user.id }}</td>
                    <td>@{{ user.first_name }}</td>
                    <td>@{{ user.last_name }}</td>
                    <td>@{{ user.email }}</td>
                    <td><span class="glyphicon glyphicon-pencil"  ng-click="toggle('edit', user.id)"></span></td>
                    <td><span class="glyphicon glyphicon-remove" ng-click="confirmDelete(user.id)"></span></td>
                </tr>
            </table>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="userModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">@{{ modal_title }}</h4>
                </div>
                <div class="modal-body">

                    <div class="timestamps">
                        <p>Created at: <span class="created-at">@{{ user.created_at }}</span></p>
                        <p>Updated at: <span class="updated-at">@{{ user.updated_at }}</span></p>
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="first-name">First Name</span>
                        <input type="text" class="form-control" aria-describedby="first_name" ng-model="user.first_name">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="last-name">Last Name</span>
                        <input type="text" class="form-control" aria-describedby="last-name" ng-model="user.last_name">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="address">Address</span>
                        <input type="text" class="form-control" aria-describedby="address" ng-model="user.address">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="city">City</span>
                        <input type="text" class="form-control" aria-describedby="city" ng-model="user.city">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="province">Province</span>
                        <input type="text" class="form-control" aria-describedby="province" ng-model="user.province">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="postal-code">Postal Code</span>
                        <input type="text" class="form-control" aria-describedby="postal-code" ng-model="user.postal_code">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="country">Country</span>
                        <input type="text" class="form-control" aria-describedby="country"ng-model="user.country">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="email">Email</span>
                        <input type="text" class="form-control" aria-describedby="email" ng-model="user.email">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="phone">Phone</span>
                        <input type="text" class="form-control" aria-describedby="phone" ng-model="user.phone">
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon" id="password">
                            Password <i class="glyphicon glyphicon-eye-open"></i>
                        </span>
                        <input type="password" id="password-field" class="form-control" aria-describedby="password" ng-model="user.password">
                    </div>
                    <p class="account-status">
                        Admin <input type="checkbox" name="is-admin" id="is-admin">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="save(modalstate, id)">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script src="../../controllers/users.js"></script>
    <script>
    /* Show Password on click */
    $(".glyphicon-eye-open").mousedown(function() {
        $('#password-field').attr('type','text');
    });
    /* Hide Password on release */
    $(".glyphicon-eye-open").mouseup(function() {
        $('#password-field').attr('type', 'password');
    });
    </script>
</body>
</html>
@stop
