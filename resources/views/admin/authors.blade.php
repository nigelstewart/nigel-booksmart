@extends('admin/layouts/crud')

@section('body')
<body ng-app="crudApp" ng-controller="authorsController">
@stop

@section('content')

<div class="content container-fluid">

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h1><?= $title ?></h1>
            <p class="add-new" ng-click="toggle('add', 0)">Add <span class="glyphicon glyphicon-plus"></span></p>
        </div>
        <div class="panel-content">
            <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-search"></i>
                <input type="text" id="search-bar" class="form-control" placeholder="Search" ng-model="searchTable"/>
            </div>
        </div>

        <!-- Table -->
        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tr ng-repeat="author in authors | filter:searchTable">
                <td>@{{ author.id }}</td>
                <td>@{{ author.name }}</td>
                <td><span class="glyphicon glyphicon-pencil" ng-click="toggle('edit', author.id)"></span></td>
                <td><span class="glyphicon glyphicon-remove" ng-click="confirmDelete(author.id)"></span></td>
            </tr>
        </table>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="authorModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@{{ modal_title }}</h4>
            </div>
            <div class="modal-body">

                <div class="timestamps">
                    <p>Created at: <span class="created-at">@{{ author.created_at }}</span></p>
                    <p>Updated at: <span class="updated-at">@{{ author.updated_at }}</span></p>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="name">Name</span>
                    <input type="text" class="form-control" aria-describedby="name" ng-model="author.name">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="country">Country</span>
                    <input type="text" class="form-control" aria-describedby="country" ng-model="author.country">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="phone">Phone</span>
                    <input type="text" class="form-control" aria-describedby="phone" ng-model="author.phone">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="email">Email</span>
                    <input type="text" class="form-control" aria-describedby="email" ng-model="author.email"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" ng-click="save(modalstate, id)">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="../../controllers/authors.js"></script>
</body>
</html>
@stop
