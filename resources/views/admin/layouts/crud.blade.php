<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>{{ $title }}</title>
    <meta name="description" content="Books Mart Admin">
    <meta name="author" content="Nigel Stewart">

    <!-- CUSTOM STYLES -->
    <link href="../../css/admin_style.css" rel="stylesheet" />

    <!-- LOAD JQUERY -->
    <script src="../../lib/jquery/jquery-3.1.1.js"></script>
    <script src="../../lib/jquery/jquery-migrate-1.2.1.js"></script>

    <!-- LOAD BOOTSTRAP -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />

    <!-- LOAD ANGULAR -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
    <script src="../../app/app.js"></script>

    <!-- LOAD FONTS -->
    <script src="https://use.fontawesome.com/c001f6b9dc.js"></script>

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
</head>

<!-- HEADER -->
@yield('body')
<nav class="navbar nav-pills navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#admin-crud" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="../../images/footer_logo.png" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="admin-crud">
            <ul class="nav navbar-nav">

                <li class="nav-item">
                    <a class="nav-link" href="/admin/panel/users">Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/panel/books">Books</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/panel/authors">Authors</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/panel/orders">Orders</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/panel/publishers">Publishers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/panel/reviews">Reviews</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/panel/categories">Categories</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Log Out</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<!-- THE PAGE CONTENT -->
@yield('content')
