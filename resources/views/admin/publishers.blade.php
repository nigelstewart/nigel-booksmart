@extends('admin/layouts/crud')

@section('body')
<body ng-app="crudApp" ng-controller="publishersController">
@stop

@section('content')

<div class="content container-fluid">

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h1><?= $title ?></h1>
            <p class="add-new" ng-click="toggle('add', 0)">Add <span class="glyphicon glyphicon-plus"></span></p>
        </div>
        <div class="panel-content">
            <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-search"></i>
                <input type="text" id="search-bar" class="form-control" placeholder="Search" ng-model="searchTable"/>
            </div>
        </div>

        <!-- Table -->
        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Publisher</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tr ng-repeat="publisher in publishers | filter:searchTable">
                <td>@{{ publisher.id }}</td>
                <td>@{{ publisher.name }}</td>
                <td><span class="glyphicon glyphicon-pencil" ng-click="toggle('edit', publisher.id)"></span></td>
                <td><span class="glyphicon glyphicon-remove" ng-click="confirmDelete(publisher.id)"></span></td>
            </tr>
        </table>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="publisherModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@{{ modal_title }}</h4>
            </div>
            <div class="modal-body">

                <div class="timestamps">
                    <p>Created at: <span class="created-at">@{{ publisher.created_at }}</span></p>
                    <p>Updated at: <span class="updated-at">@{{ publisher.updated_at }}</span></p>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="name">Publisher</span>
                    <input type="text" class="form-control" aria-describedby="name" ng-model="publisher.name">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="country">Country</span>
                    <input type="text" class="form-control" aria-describedby="country" ng-model="publisher.country">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="phone">Phone</span>
                    <input type="text" class="form-control" aria-describedby="phone" ng-model="publisher.phone">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="email">Email</span>
                    <input type="text" class="form-control" aria-describedby="email" ng-model="publisher.email"></textarea>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="website">Website</span>
                    <input type="text" class="form-control" aria-describedby="website" ng-model="publisher.website"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" ng-click="save(modalstate, id)">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="../../controllers/publishers.js"></script>
</body>
</html>
@stop
