<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title><?= $title ?></title>
  <meta name="description" content="Books Mart Admin">
  <meta name="author" content="Nigel Stewart">

  <!-- STYLES -->
  <link href="../css/admin_style.css" rel="stylesheet" />

  <!-- JQUERY -->
  <script src="../lib/jquery/jquery-3.1.1.js"></script>
  <script src="../lib/jquery/jquery-migrate-1.2.1.js"></script>

  <!-- BOOTSTRAP -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />

  <!-- CUSTOM FONTS -->
  <script src="https://use.fontawesome.com/c001f6b9dc.js"></script>

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>

<body>
    <div class="container">
        <form id="admin_login">
            <img src="../images/logo.png" />
            <div class="form-group">
                <label for="user">User</label>
                <input type="text" class="form-control" name="user"></input>
            </div>
            <div class="form-group">
                <label for="user">Password</label>
                <input type="password" class="form-control" name="password"></input>
            </div>

            <p><button type="submit" class="btn btn-default">Log In</button></p>
        </form>
    </div>
</body>
</html>
