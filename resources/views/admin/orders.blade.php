@extends('admin/layouts/crud')

@section('body')
<body ng-app="crudApp" ng-controller="ordersController">
@stop

@section('content')

<div class="content container-fluid">

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h1><?= $title ?></h1>
        </div>
        <div class="panel-content">
            <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-search"></i>
                <input type="text" id="search-bar" class="form-control" placeholder="Search" ng-model="searchTable"/>
            </div>
        </div>

        <!-- Table -->
        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Customer</th>
                <th>Order Date</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tr ng-repeat="order in orders | filter:searchTable">
                <td>@{{ order.id }}</td>
                <td>@{{ order.billing_first_name }} @{{ order.billing_last_name }}</td>
                <td>@{{ order.order_date }}</td>
                <td><span class="glyphicon glyphicon-pencil" ng-click="toggle('edit', order.id)"></span></td>
                <td><span class="glyphicon glyphicon-remove" ng-click="confirmDelete(order.id)"></span></td>
            </tr>
        </table>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="orderModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@{{ modal_title }}</h4>
            </div>
            <div class="modal-body">

                <div class="timestamps">
                    <p>Created at: <span class="created-at">@{{ order.created_at }}</span></p>
                    <p>Updated at: <span class="updated-at">@{{ order.updated_at }}</span></p>
                </div>

                <div class="input-group">
                    <span class="input-group-addon">Customer</span>
                    <span class="input-group-addon text-left">
                        @{{ order.shipping_first_name }} @{{ order.shipping_last_name }}
                    </span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon">Order Date</span>
                    <span class="input-group-addon text-left">
                        @{{ order.order_date }}
                    </span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon">GST</span>
                    <span class="input-group-addon text-left">
                        $@{{ order.gst }}
                    </span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon">PST</span>
                    <span class="input-group-addon text-left">
                        $@{{ order.pst }}
                    </span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon">Subtotal</span>
                    <span class="input-group-addon text-left">
                        $@{{ order.sub_total }}
                    </span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon">Total</span>
                    <span class="input-group-addon text-left">
                        $@{{ order.total }}
                    </span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon">Credit Card</span>
                    <span class="input-group-addon text-left">
                        *********@{{ order.credit_card_num }}
                    </span>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="shipping_status">Status</span>
                    <select class="form-control" aria-describedby="shipping_status" ng-model="order.shipping_status">
                        <option value="0">
                            Not Shipped
                        </option>
                        <option value="1">
                            Shipped
                        </option>
                    </select>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="shipping_address">Shipping Address</span>
                    <input type="text" class="form-control" aria-describedby="shipping_address" ng-model="order.shipping_address">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="city">City</span>
                    <input type="text" class="form-control" aria-describedby="city" ng-model="order.shipping_city">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="province">Province</span>
                    <input type="text" class="form-control" aria-describedby="province" ng-model="order.shipping_province">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="country">Country</span>
                    <input type="text" class="form-control" aria-describedby="country" ng-model="order.shipping_country">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="postal_code">Postal Code</span>
                    <input type="text" class="form-control" aria-describedby="postal_code" ng-model="order.shipping_postal_code">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" ng-click="save(modalstate, id)">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="../../controllers/orders.js"></script>
</body>
</html>
@stop
