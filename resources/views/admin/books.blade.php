@extends('admin/layouts/crud')

@section('body')
<body ng-app="crudApp" ng-controller="booksController">
@stop

@section('content')

<div class="content container-fluid">

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h1>Books</h1>
            <p class="add-new" ng-click="toggle('add', 0)">Add <span class="glyphicon glyphicon-plus"></span></p>
        </div>
        <div class="panel-content">
            <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-search"></i>
                <input type="text" id="search-bar" class="form-control" placeholder="Search" ng-model="searchTable"/>
            </div>
        </div>

        <!-- Table -->
        <table class="table table-striped">
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Author</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tr ng-repeat="book in books | filter:searchTable">
                <td>@{{ book.id }}</td>
                <td>@{{ book.title }}</td>
                <td>@{{ book.name }}</td>
                <td><span class="glyphicon glyphicon-pencil" ng-click="toggle('edit', book.id)"></span></td>
                <td><span class="glyphicon glyphicon-remove" ng-click="confirmDelete(book.id)"></span></td>
            </tr>
        </table>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="bookModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@{{ modal_title }}</h4>
            </div>
            <div class="modal-body">

                <div class="timestamps">
                    <p>Created at: <span class="created-at">@{{ book.created_at }}</span></p>
                    <p>Updated at: <span class="updated-at">@{{ book.updated_at }}</span></p>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="title">Title</span>
                    <input type="text" class="form-control" aria-describedby="title" ng-model="book.title">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="isbn">ISBN</span>
                    <input type="text" class="form-control" aria-describedby="isbn" ng-model="book.isbn">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="author">Author</span>
                    <select class="form-control" aria-describedby="author" ng-model="book.author_id" ng-options="author.id as author.name for author in authors | orderBy:'name'">
                        <option value="@{{ author.id }}">@{{ author.name }}</option>
                    </select>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="description">Description</span>
                    <textarea class="form-control" aria-describedby="description" ng-model="book.description"></textarea>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="publisher">Publisher</span>
                    <select class="form-control" aria-describedby="publisher" ng-model="book.publisher_id" ng-options="publisher.id as publisher.name for publisher in publishers | orderBy:'name'">
                        <option value="@{{ publisher.id }}">@{{ publisher.name }}</option>
                    </select>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="category">Category</span>
                    <select class="form-control" aria-describedby="category" ng-model="book.category_id" ng-options="category.id as category.name for category in categories | orderBy:'name'">
                        <option value="@{{ category.id }}">@{{ category.name }}</option>
                    </select>
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="publish_date">Publish Date</span>
                    <input type="text" class="form-control" aria-describedby="publish_date" ng-model="book.publish_date">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="selling_price">Selling Price</span>
                    <input type="text" class="form-control" aria-describedby="selling_price" ng-model="book.selling_price">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="cost_price">Cost Price</span>
                    <input type="text" class="form-control" aria-describedby="cost_price" ng-model="book.cost_price">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="in_stock">In Stock</span>
                    <input type="text" class="form-control" aria-describedby="in_stock" ng-model="book.copies_in_stock">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="pages">Pages</span>
                    <input type="text" class="form-control" aria-describedby="pages" ng-model="book.num_pages">
                </div>

                <div class="input-group">
                    <span class="input-group-addon" id="image">Image</span>
                    <input type="text" class="form-control" aria-describedby="image" ng-model="book.image">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" ng-click="save(modalstate, id)">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="../../controllers/books.js"></script>
</body>
</html>
@stop
