@extends('layouts.page')

@section('content')
<!-- author: Bhumi-->

@if(Session::has('message'))
<div class="alert alert-danger">{{ Session::get('message') }}
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
</div>
@endif

<div class="main_content">
    <div class="container">

        <div class="row">

            <div class="col-xs-12 col-md-4 payment-style">
                {{Form::open(array('method'=>'post','url'=>'/pay'))}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Payment Details
                            </h3>
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label for="cardNumber">
                                        CARD NUMBER</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="cardNumber" placeholder="Valid Card Number"
                                               required autofocus />
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                    </div>
                                </div>
                               <div class="form-group">
                            <label for="card_type" class="col-md-4 control-label">Card Type</label>
                             <div class="col-md-6">
                                <select name="card_type" class="form-control">
                                    <option value="mastercard">Master Card</option>
                                    <option value="visa">Visa</option>
                                    <option value="american_express">American Express</option>
                                </select>
                             </div>
                        </div>
                                <div class="row">
                                    <div class="col-xs-7 col-md-7">
                                        <div class="form-group">
                                    <label for="expiryDate">
                                        Expiry Date</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="expiryDate" placeholder="Expiry Date"
                                               autofocus />
                                    </div>
                                </div>
                                    </div>
                                    <div class="col-xs-5 col-md-5 pull-right">
                                        <div class="form-group">
                                            <label for="cvvCode">
                                                CVV CODE</label>
                                            <input type="password" class="form-control" name="cvvCode" placeholder="CVV" required />
                                        </div>
                                    </div>

                                </div>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-stacked btn btn-primary btn-block">
                        <div class="active "><span class="badge pull-right"><span class="glyphicon glyphicon-usd"></span>{{Cart::subtotal() +  number_format(Cart::subtotal() * 0.08) + number_format(Cart::subtotal() * 0.05)}}</span> Final Payment
                        </div>
                    </ul>
                    <br/>
                    <input type="submit" id="pay" name="pay" class="btn btn-success btn-lg btn-block" value="Pay" />
                {{Form::close()}}
            </div>

        </div>
    </div>
</div>
@endsection
