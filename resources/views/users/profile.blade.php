@extends ('layouts.page')

@section('content')
<!-- Author Khushboo updated by Mohit Salhotra-->
  <div class="container content">
      <div class="row">
      <div class="col-xs-11 col-sm-11">


    {!! Form::open( array( 'method'=>'post','url' => url('/profile')) ) !!}
      <fieldset>
          <div class="row margin_left_10">
              <div class="col-xs-12 col-sm-12">


        <legend>Edit Profile</legend>


          {!! Form::hidden('id',$users->id) !!}


         <p>{!! Form::label('first_name','First Name:') !!}
               {!! Form::text('first_name',($users->first_name), array('class' => 'form-control')) !!}</p>

          <p>  {!! Form::label('last_name','Last Name:') !!}
             {!! Form::text('last_name',($users->last_name), array('class' => 'form-control'))  !!}</p>

          <p> {!! Form::label('address','Address:') !!}
          {!! Form::text('address',($users->address), array('class' => 'form-control'))  !!}</p>

           <p>  {!! Form::label('city','City:') !!}
          {!! Form::text('city',($users->city), array('class' => 'form-control'))  !!}</p>

          <p> {!! Form::label('postal_code','Postal Code:') !!}
          {!! Form::text('postal_code',($users->postal_code), array('class' => 'form-control'))  !!}</p>


          <p>    {!! Form::label('province','Province:') !!}
          {!! Form::text('province',($users->province), array('class' => 'form-control'))  !!}</p>

          <p>  {!! Form::label('country','Country:') !!}
          {!! Form::text('country',($users->country), array('class' => 'form-control'))  !!}</p>

          <p> {!! Form::label('phone','Phone:') !!}
          {!! Form::text('phone',($users->phone), array('class' => 'form-control')) !!}</p>

          <p>  {!! Form::label('email','Email:') !!}
          {!! Form::text('email',($users->email), array('class' => 'form-control')) !!}</p>

          <p>
            {!! Form::submit( 'Update', array('class' => 'button btn btn-success')) !!}
          </p>

          </div><!--col-->
          </div><!--row-->

   </fieldset>
   {!! Form::close() !!}




  </div><!-- /col -->
  </div>
    </div><!-- /container -->
@stop
