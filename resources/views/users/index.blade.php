@extends('layouts.page')

@section('content')


<!--Author:Khushboo Batish, updated and commented by Mohit Salhotra-->
<div class="container">
    <h1 class="margin_left_10">User Profile</h1>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <p> <span class="green_tag margin_left_10 padding_10">Personal Details</span></p>
        <div class="margin_left_10 padding_10">

        <p><strong>First Name</strong>
            {{ $users->first_name }}</p>
        <p><strong>Last Name:</strong>
            {{ $users->last_name }}</p>
        <p><strong>Phone:</strong>
            {{ $users->phone}}</p>
        <p><strong>Email:</strong>
            {{ $users->email }}</p>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 ">
        <p> <span class="green_tag margin_left_10 padding_10">Address Details</span></p>

        <div class="margin_left_10 padding_10">
        <p><strong>Address:</strong>
            {{ $users->address}}</p>
        <p><strong>City:</strong>
            {{ $users->city}}</p>
        <p><strong>Postal_code:</strong>
            {{ $users->postal_code }}</p>
        <p><strong>Province:</strong>
            {{ $users->province }}</p>
        <p><strong>Country:</strong>
            {{ $users->country}}</p>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="btn margin_left_10 btn-small btn-success" ><a href="{{ URL::to('profile/' . $users->id . '/edit') }}">Edit Profile</a></div>
    </div>


</div><!--row-->
</div><!--container-->

@endsection
