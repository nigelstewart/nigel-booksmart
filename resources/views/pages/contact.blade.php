@extends ('layouts.page')
@section('content')


<div class="container">
    <div class="row">
        <div class="col-sm-6 col-xs-12">
                {{ Form::open(array('url' => 'contact', 'id' => 'form_fields')) }}
                        <h1>Contact Us</h1>
                        {!! Form::label('user', 'User Name') !!}
                        {!! Form::text('user') !!} <br />
                        {!! Form::label('email', 'E-Mail') !!}
                        {!! Form::text('email') !!}<br />
                        {!! Form::label('phone', 'Phone') !!}
                        {!! Form::text('phone') !!}<br />
                        {!! Form::label('comment', 'Comment') !!}
                        {!! Form::text('comment') !!}<br />
                        {!! Form::submit( 'Submit', array('id' => 'low-padding','class' => 'button btn btn-primary' )) !!}

                    {{ Form::close() }}
        </div>
        <div class="col-sm-6 image_map">
            <h2>Our Location</h2>
            <img src="images/map.jpg" class="img-responsive" alt="map">
        </div>
        <div class="col-sm-6">

        </div>
        <div class="col-sm-6 contact_info">
            <h2>How To Reach Us</h2>
            <p><strong>General Information: </strong>(204)100-0000</p>
            <p><strong>Email: </strong>contact@booksmart.ca</p>
            <p><strong>Address: </strong>Winnipeg, MB</p>
        </div>
    </div>
</div>

@stop
