@extends('layouts/page')

@section('content')
<div class="container content">
    <div class="row banner">
        <img class="col-xs-12" src="images/books_banner.jpg" alt="Books Banner" />
    </div>

     <!--Category loop start here-->
    @foreach($allbooks as $key => $category)

    <div class="row category">
      <div class="col-xs-12">
        <a href="/category/{{ $key }}" class="genre-link">{{$key}}</a>
        <a href="/category/{{ $key }}" class="view-more">View More &gt;&gt;</a>
      </div>
    </div>


    <div class="row">
        <div class="slider autoplay col-xs-12">
        @foreach($category as $book)
            <div><a href="/book_detail/{{$book->id}}"><img src="/images/books/{{$book->image}}" alt="{{$book->title}}" /></a></div>
        @endforeach
        </div>
    </div>
    @endforeach
</div>
    <!--Category loop ends here-->

<script type="text/javascript">
$(document).ready(function(){
    $('.autoplay').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        dots: true,
        responsive: [
            {
                breakpoint: 860,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    arrows: false,
                    dots: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: false,
                    dots: true
                }
            }
        ]
    });
});
</script>

@stop
