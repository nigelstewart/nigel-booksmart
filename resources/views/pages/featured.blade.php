@extends ('layouts.page')
@section('content')

<!--
    Author: Sanders
-->
<div class="container">
    <div id="back_content" class="row">
        <h3>Featured</h3>
        <div class="col-xs-12">
            <div id="category_detail_content" class="row">
                @foreach($books as $book)
                <div class="col-xs-12 col-sm-4">
                    <div class="area_category_content">
                        <div class="category_detail_headings">
                            <div id="bookpadding"><span id="category_detail_head">{{ $book['title']}}</span></div>
                        </div>
                        <div id="category_images" ><div id="bookpadding"><a href="/book_detail/{{$book->id}}"><img class="category_image" src="/images/books/{{$book->image}}" alt="{{$book->title}}" /></a></div></div>
                    </div>
                </div><!-- /col-xs-12-->
                @endforeach
            </div><!-- /category_detail -->
        </div><!-- /col-xs-12 col-sm-10 -->
    </div><!-- /row -->
</div><!-- /container -->
@stop
