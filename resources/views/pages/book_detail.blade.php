@extends('layouts.page')

@section('content')
<!--
    author: Bhumi
    Updated By: Sanders
-->
<div class="main_content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            <div class="col-xs-12 col-sm-5" id="detail_image">
                <div class="image_detail_padding">
                    <img src="/images/books/{{$book['image']}}" alt="Book detail" />
                </div><!-- image_detail_padding-->
                <div><br />
                    <form action='/cart/add' id='cart_form' method='get'>
                        <label id="qty"><strong>Quantity</strong></label>
                        <!--Display according to available stock-->
                        <select name="quantity" class="quantity">
                            @for ($i=1; $i<=10; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select><br/>
                        <input type="hidden" name="id" value="{{$book['id']}}" />
                        <input type="submit" name="add_to_cart" id="green_button" class="button btn btn-primary" value="Add to Cart"/>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7" id="detail_info">
                <div>
                    <h3>{{ $book['title']}}</h3>
                    <p>{{ $book['publish_date']}}</p>
                    <p>By <strong><a href="/author_detail/{{$book['author_id']}}">{{ $book['auth_name'] }}</a></strong></p>
                    <p>Price: <strong>${{$book['selling_price']}}</strong></p>
                    <p><b>Description:</b></p>
                    <p>{{ $book['description'] }}</p>
                </div><br/>
                <div class="product_detail">
                    <h3>Product Details </h3>
                    <p><strong>Publisher: </strong><a href="/publisher_detail/{{$book['publisher_id']}}">{{ $book['pub_name'] }}</a></p>
                    <p><strong>Author: </strong>{{ $book['auth_name']}}</p>
                    <p><strong>Category: </strong>{{ $book['category_id']}}</p>
                    <p><strong>ISBN: </strong>{{$book['isbn']}}</p>
                    <p><strong>Language: </strong>English</p>
                    <p><strong>Number of Reviews: </strong>{{ count($review) }}</p>
                    <p><strong>Copies In Stock: </strong>{{$book['copies_in_stock']}}</p>
                </div><!-- product_detail-->
             </div><!--detail_info-->
            </div><!-- col-xs-12-->
        </div><!-- /row-->

        <div class="row">
            <!--
                Author: Hemali
                Updated By: Sanders
            -->
            @if(Auth::check())
            <div class="col-xs-12 review">
                <div class="col-xs-12 review-form">
                    <form id="review_form" class="image_detail_padding" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{Auth::user()->id}}" name="user_id" />
                        <input type="hidden" value="{{ $book['id'] }}" name="book_id" />
                        <p><label for="review">Write a Review: </label></p>
                        <textarea form="review_form" name="message" id="review"></textarea>
                        <span class="star-cb-group">
                            <input type="radio" id="rating-5" name="stars" value="5" /><label for="rating-5">5</label>
                            <input type="radio" id="rating-4" name="stars" value="4" checked="checked" /><label for="rating-4">4</label>
                            <input type="radio" id="rating-3" name="stars" value="3" /><label for="rating-3">3</label>
                            <input type="radio" id="rating-2" name="stars" value="2" /><label for="rating-2">2</label>
                            <input type="radio" id="rating-1" name="stars" value="1" /><label for="rating-1">1</label>
                            <input type="radio" id="rating-0" name="stars" value="0" class="star-cb-clear" /><label for="rating-0">0</label>
                        </span>
                        <p><button id="low-padding" class="button btn btn-primary" type="submit">Submit</button></p>
                    </form>
                </div>
            @else
            <div class="col-xs-12 review">
                <div class="col-xs-12 review-form no-review">
                    <p class="green">You need to be logged in to write a review</p>
                    <p><a href="/login">Log In</a> &#124; <a href="/register">Sign Up</a></p>
                </div>
            @endif

            @if(empty($review))
                <h3>Reviews</h3>
                @foreach($review as $key)
                <blockquote>
                  <p>{{ $key['message'] }}</p>
                  <br />
                  <p><b>Stars:</b> {{ $key['stars'] }}</p>
                  <footer>{{ $key['first_name'] }}&nbsp;{{ $key['last_name'] }}, {{$key['created_at']}}</footer>
                </blockquote>
                @endforeach
            @else
                <h3>Sorry there are no reviews for this book.</h3>
            @endif
            </div><!-- col-xs-12 review-->
        </div><!-- end row-->
    </div><!-- container-->
</div><!-- main-content-->

@endsection
