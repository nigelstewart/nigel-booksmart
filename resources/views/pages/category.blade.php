@extends ('layouts.page')

@section('content')


    <!--created by Khushboo Batish-->
      <div class="container content">
         <div class="hidden-xs col-sm-2 sidebar_color">
         <h3>Categories</h3>
             <ul id="category_nav">
                <li class="cat-nav"><a href="">Horror</a></li>
                <li class="cat-nav"><a href="">Romance</a></li>
                <li class="cat-nav"><a href="">Fiction</a></li>
                <li class="cat-nav"><a href="">Comedy</a></li>
                <li class="cat-nav"><a href="">Fantasy</a></li>
            </ul>
         </div><!-- hidden-xs col-xs-2-->
         <h3 class="green_bar">Fiction</h3>
         <div class="col-xs-12 col-sm-10">
            <div class="row spacedown row-centered">

           <div class="col-xs-6 col-sm-4 .col-centered">
             <img class=" responsive_image" src="images/black.png" alt="black book"/>
             <h3>Black</h3>
             <div class="pclass">
             <p>By Author</p>
             <p> <img src="images/tick.png" alt="black book" width="15"/>In Stock</p>
             <p><strong>Price: &#36;20</strong></p>
             <p>Reviews: 2</p>
             </div><!-- pclass-->
           </div><!-- col-xs-12 col-sm-4-->

             <div class="col-xs-6 col-sm-4">
             <img class=" responsive_image" src="images/suffering.jpg" alt="The Suffering"/>
              <h3>Suffering</h3>
              <div class="pclass">
             <p>By Author</p>
             <p><img src="images/tick.png" alt="black book" width="15"/>In Stock</p>
             <p><strong>Price: &#36;20</strong></p>
             <p>Reviews: 2</p>
             </div><!-- pclass-->
           </div><!-- col-xs-12 col-sm-4-->


             <div class="col-xs-6 col-sm-4 ">
             <img class=" responsive_image" src="images/horror.jpg" />
              <h3>Horror</h3>
              <div class="pclass">
              <p>By Author</p>
             <p><img src="images/tick.png" alt="black book" width="15"/>In Stock</p>
              <p><strong>Price: &#36;20</strong></p>
             <p>Reviews: 2</p>
             </div><!-- pclass-->
           </div><!-- col-xs-12 col-sm-4-->

           <div class="col-xs-6 col-sm-4">
             <img class=" responsive_image" src="images/replacement.jpg" />
             <h3>Replace</h3>
             <div class="pclass">
              <p>By Author</p>
             <p><img src="images/tick.png" alt="black book" width="15"/>In Stock</p>
              <p><strong>Price: &#36;20</strong></p>
             <p>Reviews: 2</p>
             </div><!-- pclass-->
           </div><!-- col-xs-12 col-sm-4-->

             <div class="col-xs-6 col-sm-4">
             <img class=" responsive_image" src="images/lips.jpg" />
              <h3>Lips</h3>
              <div class="pclass">
              <p>By Author</p>
             <p><img src="images/tick.png" alt="black book" width="15"/>In Stock</p>
              <p><strong>Price: &#36;20</strong></p>
             <p>Reviews: 2</p>
             </div><!-- pclass-->
           </div><!-- col-xs-12 col-sm-4-->


             <div class="col-xs-6 col-sm-4">
             <img class=" responsive_image" src="images/dead.jpg" />
              <h3>Dead</h3>
              <div class="pclass">
            <p>By Author</p>
             <p><img src="images/tick.png" alt="black book" width="15"/>In Stock</p>
              <p><strong>Price: &#36;20</strong></p>
             <p>Reviews: 2</p>
             </div><!-- pclass-->
           </div><!-- col-xs-12 col-sm-4-->
           </div><!--end of row-->
         </div>

      </div>
@stop
