@extends('layouts.page')

@section('content')
<!-- Author: Hemali -->
<div class="container">
    <div class="row">
        <div class="col-xs-12">

                    <h1>PRIVACY</h1>
                        <h2>Our Privacy Policy</h2>
                    <ul>


    <li><h3>1.WHAT INFORMATION IS COLLECTED BY BOOKSMART?</h3>
      <p>Any time you make a purchase, enter a contest, complete a ballot or otherwise provide personal information to Booksmart, it is collected, used and disclosed by Bench for various purposes as described herein. Moreover, every time you visit the Site, the following information is automatically obtained and compiled: your internet protocol (IP) address and the domain name registry information associated with that IP address; identification of your internet browser, operating system and hardware platform you are using; the date and time of your visit; the name and address (URL) of the web page you viewed immediately prior to accessing the Site; your entire query (if any) that was entered into a search engine which led you to the Site; and any other “click stream” data.</p></li>



    <li><h3>2.HOW IS MY PERSONAL INFORMATION USED?</h3>
      <p>In order to better serve you, we may combine information you give us online, in our stores, by telephone or in writing about your product interests. We may also combine these with readily available demographic information in order to enable us to communicate with you, by telephone (including by way of automated dialing answering device) e-mail, mail or otherwise, about our products, services, surveys, promotions, special offers, and contests.</p></li>



    <li><h3>3.TO WHOM MAY MY PERSONAL INFORMATION BE DISCLOSED?</h3>
      <p>We will not disclose your personal information to any non-affiliated third party without your prior consent, other than as provided below: We may disclose your personal information to any Bench Affiliates, so that it may communicate with you (whether by e-mail, telephone, post or otherwise) and provide you with promotional materials and additional information about its products, services, promotions and special offers that may be of interest to you, or collect and use that information subject to and for other purposes contemplated by this Policy.</p></li>

    <li><h3>4.HOW SECURE IS MY PERSONAL INFORMATION?</h3>
      <p>We take great care in trying to maintain the security of personal information we have collected. We have adopted physical and technological processes and procedures to protect the confidentiality of personal information and to safeguard personal information against loss or theft, as well as unauthorized access, disclosure, copying, use or modification, in light of, among other things, the sensitivity of the information and the purposes for which it is to be used.</p></li>



    <li><h3>5.PRIVACY POLICY CHANGES</h3>
      <p>If we decide to change our Policy, we will post these changes on our Site so that you can be updated as to what information we gather, how that information may be used and whether (and how) it may be disclosed to any third parties. Following the posting of such changes, you will be bound by such changes unless you advise us in writing, by phone or by e-mail that you wish to withdraw your consent (as per the procedure outlined above).</p></li>

      <li><h3>6.HOW DO I EXERCISE MY CHOICES ABOUT RECEIVING PROMOTIONAL COMMUNICATIONS?</h3>
      <p>You always have the right to direct us not to share your information with third parties (see “How do I exercise my choices about the collection, use and disclosure of my personal information?” below). With regard to the collection and disclosure of aggregate information however, please note that you will not be able to opt out as this information simply reflects statistical analysis of various aspects of Site usage and does not include personally identifiable information.</p></li>


    </ul>
        </div>
    </div>
</div>
@endsection
