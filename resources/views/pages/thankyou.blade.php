@extends('layouts.page')

@section('content')
<!-- author: Bhumi Patel-->
<div class="main_content">
    <div class="container" style="padding:20px;">
      <div class="row">
      <div class="col-xs-12">
          <div class="text-center">
            <h1>Thank you for shopping with Books Mart</h1>
              <h2>Invoice for purchase</h2>
          </div>
          <hr>
          <div class="row">
              <div class="col-xs-12 col-md-6 col-lg-6 pull-left">
                  <div class="panel panel-default height">
                      <div class="panel-heading">Billing Details</div>
                      <div class="panel-body">
                          <strong>{{$order->billing_first_name}} {{$order->billing_last_name}}</strong><br>
                          {{$order->billing_address}}<br>
                          {{$order->billing_city}}<br>
                          {{$order->billing_province}}<br>
                          {{$order->billing_country}}<br>
                          <strong>{{$order->billing_postal_code}}</strong><br>
                      </div>
                  </div>
              </div>
              <div class="col-xs-12 col-md-6 col-lg-6 pull-right">
                  <div class="panel panel-default height">
                      <div class="panel-heading">Shipping Address</div>
                      <div class="panel-body">
                          <strong>{{$order->shipping_first_name}} {{$order->shipping_last_name}}</strong><br>
                          {{$order->shipping_address}}<br>
                          {{$order->shipping_city}}<br>
                          {{$order->shipping_province}}<br>
                          {{$order->shipping_country}}<br>
                          <strong>{{$order->shipping_postal_code}}</strong><br>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <h3 class="text-center"><strong>Order summary</strong></h3>
              </div>
              <div class="panel-body">
                  <div class="table-responsive">
                      <table class="table table-condensed">
                          <thead>
                              <tr>
                                  <td><strong>Item Name</strong></td>
                                  <td class="text-center"><strong>Item Price</strong></td>
                                  <td class="text-center"><strong>Item Quantity</strong></td>
                                  <td class="text-right"><strong>Total</strong></td>
                              </tr>
                          </thead>
                          <tbody>
                            <?php foreach($cart_temp as $row) :?>
                              <tr>
                                  <td>{{$row->name}}</td>
                                  <td class="text-center">${{$row->price}}</td>
                                  <td class="text-center">{{$row->qty}}</td>
                                  <td class="text-right">${{$row->qty * $row->price}}</td>
                              </tr>
                            <?php endforeach; ?>

                              <tr>
                                  <td class="highrow"></td>
                                  <td class="highrow"></td>
                                  <td class="highrow text-center"><strong>Subtotal</strong></td>
                                  <td class="highrow text-right">${{$order->sub_total}}</td>
                              </tr>
                              <tr>
                                  <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                  <td class="emptyrow"></td>
                                  <td class="emptyrow text-center"><strong>GST</strong></td>
                                  <td class="emptyrow text-right">${{$order->gst}}</td>
                              </tr>
                              <tr>
                                  <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                  <td class="emptyrow"></td>
                                  <td class="emptyrow text-center"><strong>PST</strong></td>
                                  <td class="emptyrow text-right">${{$order->pst}}</td>
                              </tr>
                              <tr>
                                  <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                  <td class="emptyrow"></td>
                                  <td class="emptyrow text-center"><strong>Total</strong></td>
                                  <td class="emptyrow text-right">${{$order->total}}</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    </div>

</div>
</div>

@endsection
