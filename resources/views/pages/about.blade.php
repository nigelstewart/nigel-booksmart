@extends ('layouts.page')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <h1>About Us</h1>

            <hr />

            <p>
                Books Mart&trade; had been in the business for 50+ years, starting off as
                a simple mom and pop shop and slowly growing into our competitive state. We
                value you and your reading habits. If you just need a book while you sit on the can
                or if you are an avid reader who reads on the can multiple times a day,
                Books Mart&trade; has got you covered.
            </p>

            <p>
                We are a company that is commited to providing readers with a source
                of not only entertainment but a lifestyle. We have provided over ten thousand
                clients with reading and entertainment. From Fifty Shades of Grey to Captain Underpants
                we allow both kids and parents to enjoy their favourite reads.
            </p>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Peccata paria. Claudii libidini, qui tum erat summo ne imperio, dederetur. Qua tu etiam inprudens utebare non numquam. Idemne potest esse dies saepius, qui semel fuit? Duo Reges: constructio interrete. At certe gravius. Quodsi ipsam honestatem undique pertectam atque absolutam. Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster? An potest, inquit ille, quicquam esse suavius quam nihil dolere? Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Sic enim censent, oportunitatis esse beate vivere.

Oratio me istius philosophi non offendit; Summum a vobis bonum voluptas dicitur. Quae animi affectio suum cuique tribuens atque hanc, quam dico. Certe nihil nisi quod possit ipsum propter se iure laudari. Summum ením bonum exposuit vacuitatem doloris</p>

        </div>
    </div>
</div>


@stop
