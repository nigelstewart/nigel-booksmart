@extends('layouts.page')

@section('content')
<!-- Author: Hemali -->
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Frequently Asked Questions</h1>

            <h2>Joining Booksmart</h2>
            <ol>

            <li><h3>How do I create an account on Booksmart?</h3>
              <p>To create a Booksmart account, click Sign Up. From there, you can create a new account from scratch using your email</li>



            <li><h3>What desktop web browsers do you support?</h3>
              <p>For the best experience on Booksmart, we recommend you use the latest versions of Google Chrome, Mozilla Firefox, Microsoft Edge, Internet Explorer, or Safari.</li>



            <li><h3>What mobile web browsers do you support?</h3>
              <p>We officially support Mobile Safari on iOS, Google Chrome for iOS, Google Chrome for Android and Firefox for Android. We are working hard to add support for more mobile browsers.</li>



            <li><h3>How do I add comments and tags to my links?</h3>
              <p>When saving a bookmark, you are encouraged to include a comment and tags to better categorize and contextualize why you're adding the page to your collection. Tags can be one word or many, with commas used between each tag to designate where tags begin and end. If a link has already been saved, click 'Edit' (next to it) to add additional tags or update your comments. Tags and comments can be edited, renamed, or deleted at any time.</li>



              <li><h3>How do I edit or delete my bookmarks?</h3>
              <p>Once you've added a bookmark, you can always go back to rename the title, add or edit your tags and comments, change its privacy setting, or delete it. Click 'My Links' to access your collection of bookmarks, then edit or delete individual bookmarks by clicking 'Edit Bookmark' or 'Delete Bookmark' for each item. You can also bulk edit multiple items by checking the items you wish to edit, and then selecting the appropriate action at the top of the list.</li>


            </ol>
        </div>
    </div>
</div>
@endsection
