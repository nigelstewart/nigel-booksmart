@extends('layouts.page')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">

                          <h1>Terms and Conditions</h1>
                        <h2>Joining Booksmart</h2>
    <ol>


    <li><h3>USE OF THIS WEBSITE</h3>
      <p>This booksmart.local website is owned and operated by Freemark  doing business as books (hereinafter, collectively the “Company”), for your personal and non-commercial use and information. Your use of this website is subject to the following terms and conditions of use and sale (“Terms”) and all applicable laws. By accessing and browsing this website, you accept, without limitation or qualification, these Terms. If you do not agree with any of the Terms, please do not use this website</p></li>



    <li><h3>PRODUCT PURCHASES AND USER ACCOUNT</h3>
      <p>If you register on the site for the purchase of books,you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer and your account, and you agree to accept responsibility for all activities that occur under your account or password. The Company reserves the right to refuse service, terminate accounts or cancel orders in its sole discretion   </p></li>



    <li><h3>BOOKS INFORMATION AND AVAILABILITY</h3>
      <p>books which may be purchased from this website are available for sale and distribution to customers in Canada only. The Company attempts to be as accurate as possible in describing all Products available for sale and/or distribution by the Company. However, the Company does not warrant that Product descriptions or other content of this website are accurate, complete, reliable, current or error-free.</p></li>



    <li><h3>BOOKS ORDERS</h3>
      <p> The Company is pleased to accept orders provided they are for the personal, non-commercial use by its customers. The Company will not accept orders which are intended for resale or distribution for commercial purposes. The resale of any Products purchased on the website without the express written agreement of the Company is not permitted.  </p></li>



    <li><h3>PRODUCT PRICING</h3>
      <p> All prices and books orders are quoted and shall be processed in Canadian dollars. Although the Company strives to provide accurate product and pricing information, errors may occur. The Company reserves the right to correct any errors in pricing or product information and to modify the prices of Products, at any time, without prior notice.</p></li>



    <li><h3>CANCELLATION</h3>
      <p>The Company reserves the right, in its sole discretion, to limit quantities, terminate accounts and to refuse or cancel any order, including after the order has been submitted, whether or not the order has been confirmed or accepted and your credit card charged. In the event that your order is cancelled after your payment has been processed, the Company will issue a full refund </p></li>



      <li><h3>PAYMENT TERMS</h3>
      <p>Terms of payment for any Products purchased through this website shall be determined at the Company’s sole discretion. Payment shall be made by credit card unless some other pre-arranged method of payment has been accepted by the Company. Any payments made by credit card are subject to the approval of the financial institution that has issued the credit card</p></li>



        <li><h3>SHIPPING AND TAXES</h3>
        <p>The Company will ship the books(s) ordered by you according to the delivery method you have chosen and to the address indicated in the Order Confirmation. Express delivery for Canadian orders is 1-2 business days and Standard Delivery is 4-7 business days, and longer in the case of remote locations</p></li>



      <li><h3>DAMAGE TO OTHERS</h3>
      <p>You agree not to introduce into or through this website or any other Company website any information or materials which may be harmful to others. Among other things, you agree not to include, knowingly or otherwise, any error or defect in material or information which may, among other things, be a libel, slander, defamation or obscenity, or promote hatred or otherwise give rise to a criminal offence or civil liability on the part of any person or entity.</p><td>


     <li><h3>MODIFICATION OF WEBSITE; RESERVATION OF RIGHTS</h3>
     <p>Company may, for any reason in its sole discretion and without notice to you, terminate, change, suspend or discontinue this website or any aspect of it, and Company will not be liable to you or any third party for doing so. Company may also impose rules for and limits on use of this website or restrict your access to part, or all, of this website without notice or liability. All rights not expressly granted in these terms are reserved to the Company</p></li>



      <li><h3>GIFT CARDS</h3>
      <p>How to use your booksmart gift card:Visit Boooksmart.local and select the item(s) you wish to purchase. Upon checkout, simply enter the unique gift card number above in the field indicated. The dollar value of the gift card will automatically be applied to your purchase total.This gift card may only be applied towards online purchases made at bench.ca. It cannot be applied to purchases made in a Bench retail store. All gift card purchases are subject to credit card verification within 72 hours of purchase</p></li>


    </ul>
        </div>
    </div>
</div>
@endsection
