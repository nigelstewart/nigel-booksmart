@extends ('layouts.page')
@section('content')

<!--created by Parminder Grewal-->
<div class="container">
  <div id="back_content" class="row">
        <div class="hidden-xs col-sm-2 no-left-margin sidebar_color">
            <h3>Categories</h3>
            <ul id="category_nav">
            @foreach($categories as $category)
                <li class="cat-nav"><a href="/category/{{ $category->name }}">{{ $category->name }}</a></li>
            @endforeach
            </ul>

        </div><!-- hidden-xs col-xs-2-->

        <div class="col-xs-12 col-sm-10 ">
            <div id="category_detail_content" class="row">
                @foreach($books as $book)
                <div class="col-xs-12 col-sm-6 col-md-4 ">
                    <div class="area_category_content">
                        <div class="category_detail_headings">
                    <div id="bookpadding"><span id="category_detail_head">{{ $book['title']}}</span></div>
                        </div>
                    <div id="category_images" ><div id="bookpadding"><a href="/book_detail/{{$book->id}}"><img class="category_image" src="/images/books/{{$book->image}}" alt="{{$book->title}}" /></a></div></div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>


        </div>

 <!--books loop start here-->

  </div>
@stop
