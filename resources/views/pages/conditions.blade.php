@extends('layouts.page')

@section('content')
<!-- Author: Hemali -->
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>CONDITIONS OF USE</h1>

            <h2>Our Conditions </h2>

        <ul>

            <li><h3>Prevent Abuses</h3>
              <p>This is the agreement that sets the rules and guidelines that users must agree to and follow in order to use and access your website or mobile app. The Privacy Policy agreement informs users what kind of data you collect and how you are using that data.</p>
            </li>



            <li><h3>2.Own Your Content</h3>
              <p>As the website owner, you’re the owner of your logo, content (except for user-generated content, as most websites will inform users that any content created by users is theirs), the design of the website, and so on.</p>
            </li>


            <li><h3>3.Terminate Accounts</h3>
              <p>Prevent Abuses suggested that you could temporarily ban users, another common clause that Terms and Conditions agreements include is the Termination clause.</p>
            </li>


            <li><h3>4.Limit Liability</h3>
              <p>Terms and Conditions agreements commonly include a warranty disclaimer that tries to limit the website owner’s liability in cases where errors are found in the content presented on the website.</p>
            </li>


              <li><h3>5.Set The Governing Law</h3>
               <p>Usually, the Governing Law clause of a Terms and Conditions agreement refers to the jurisdiction that applies to the terms presented in the agreement.</p>
              </li>
          </ul>
        </div>
    </div>
</div>
@endsection
