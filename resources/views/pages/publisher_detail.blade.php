@extends('layouts.page')

@section('content')
<!-- author: Bhumi-->
<div class="main_content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-7" id="detail_info">
                                <div>
                                    <h1>Publisher Details</h1>
                                    <h3>{{$publisher['name']}}</h3>
                                    <p><strong>Publisher's Country:</strong> {{$publisher['country']}}</p>
                                    <p><strong>Publisher's Phone:</strong> {{$publisher['phone']}} </p>
                                    <p><strong>Publisher's Email Address:</strong> {{$publisher['email']}}</p>
                                    <p><strong>Publisher's Website:</strong> {{$publisher['website']}}</p>

                                </div><br/>
                             </div><!--detail_info-->
                        </div><!-- /row-->

                        <p><strong>Other books by this publisher</strong></p>
                            <div class="row">
                                <div class="slider autoplay col-xs-12">
                                    @foreach($books as $book)
                                        <div><a href="/book_detail/{{$book->id}}"><img src="/images/books/{{$book->image}}" alt="{{$book->title}}" /></a></div>
                                    @endforeach
                                </div><!-- slider autoplay-->
                            </div><!-- end row-->

                    </div><!-- container-->
                </div><!-- main-content-->
<script type="text/javascript">
$(document).ready(function(){
    $('.autoplay').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        dots: true,
        responsive: [
            {
                breakpoint: 860,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    arrows: false,
                    dots: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: false,
                    dots: true
                }
            }
        ]
    });
});
</script>
@endsection
