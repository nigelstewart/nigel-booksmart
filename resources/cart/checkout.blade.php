@extends('layouts.page')

@section('content')
<!-- author: Bhumi-->
<div class="main_content">
    <div class="container">

        <h1>Books Mart Checkout</h1>
        <hr/>

        <strong>Purchase Summary:</strong>
        <h2 class="bg-success" style="background-color: #5ea928; color: #fff;">Today's Total: ${{Cart::total()}} </h2>
        {{Form::open(array('method'=>'post','url'=>'/payment'))}}
        <div class="form-group col-md-12 bg-primary">
            <label class="control-label" for="billinginformation">Shipping Information</label>
        </div>

        <div class="shipping-info">
            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="firstname">First Name</label>
                <div class="controls">
                    <input id="ship_first_name" name="ship_first_name" type="text" value="{{Auth::user()->first_name}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="lastname">Last Name</label>
                <div class="controls">
                    <input id="ship_last_name" name="ship_last_name" type="text" value="{{Auth::user()->last_name}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="shippingaddress">Shipping Address</label>
                <div class="controls">
                    <input id="shippingaddress" name="shippingaddress" type="text" value="{{Auth::user()->address}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="shippingstate">Shipping Province</label>
                <div class="controls">
                    <input id="shippingstate" name="shippingstate" type="text" value="{{Auth::user()->province}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="shippingcountry">Shipping Country</label>
                <div class="controls">
                    <input id="shippingcountry" name="shippingcountry" type="text" value="{{Auth::user()->country}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="shippingcity">Shipping City</label>
                <div class="controls">
                    <input id="shippingcity" name="shippingcity" type="text" value="{{Auth::user()->city}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="ship_postcode">Postal Code</label>
                <div class="controls">
                    <input id="ship_postcode" name="ship_postcode" type="text" value="{{Auth::user()->postal_code}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-12 bg-primary">
                <label class="control-label" for="billinginformation">Billing Address</label>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="bill_first_name">First Name</label>
                <div class="controls">
                    <input id="bill_first_name" name="bill_first_name" type="text" value="{{Auth::user()->first_name}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="bill_last_name">Last Name</label>
                <div class="controls">
                    <input id="bill_last_name" name="bill_last_name" type="text" value="{{Auth::user()->last_name}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="billingaddress">Billing Address</label>
                <div class="controls">
                    <input id="billingaddress" name="billingaddress" type="text" value="{{Auth::user()->address}}" class="form-control">
                </div>
            </div>


            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="billingcountry">Billing Country</label>
                <div class="controls">
                    <input id="billingcountry" name="billingcountry" type="text" value="{{Auth::user()->country}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="billingprovince">Billing Province</label>
                <div class="controls">
                    <input id="billingprovince" name="billingprovince" type="text" value="{{Auth::user()->province}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="billingcity">Billing City</label>
                <div class="controls">
                    <input id="billingcity" name="billingcity" type="text" value="{{Auth::user()->city}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="billingpostcode">Post Code</label>
                <div class="controls">
                    <input id="billingpostcode" name="billingpostcode" type="text" value="{{Auth::user()->postal_code}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-12 bg-primary">
                <label class="control-label" for="contactinformation">Contact Information:</label>
            </div>

            <div class="form-group col-md-6">
                <span class="required-lbl">* </span><label class="control-label" for="emailaddress">Email Address</label>
                <div class="controls">
                    <input id="email" name="email" type="email" value="{{Auth::user()->email}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label class="control-label" for="phone">Phone</label>
                <div class="controls">
                    <input id="phone" name="phone" type="text" value="{{Auth::user()->phone}}" class="form-control">
                </div>
            </div>

            <div class="form-group col-md-12">
                <div class="control-group confirm-btn">
                    <label class="control-label" for="placeorderbtn"></label>
                    <div class="controls">
                        <input type="submit" id="placeorderbtn" name="placeorderbtn" class="btn btn-primary" value="Proceed to Payment" />
                    </div>
                </div>
            </div>

        </div>
        {{Form::close()}}
    </div>
</div>
@endsection
