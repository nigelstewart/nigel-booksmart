@extends('layouts.page')

@section('content')
<!-- author: Bhumi-->
<div class="main_content">
<div class="container">
    <?php if(Cart::count() !== 0):?>
	<table id="cart" class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width:50%">Product</th>
                    <th style="width:10%">Price</th>
                    <th style="width:8%">Quantity</th>
                    <th style="width:22%" class="text-center">Subtotal</th>
                    <th style="width:10%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach(Cart::content() as $row) :?>
                    <tr>
                            <td data-th="Product">
                                    <div class="row">
                                            <div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
                                            <div class="col-sm-10">
                                                    <h4 class="nomargin">{{$row->name}}</h4>
                                                    <!--<p>{{$row->options->description}}</p>-->
                                            </div>
                                    </div>
                            </td>
                            <td data-th="Price">${{$row->price}}</td>

                            <td data-th="Quantity">
                                <form action="/cart/update" method="get">
                                    <input type="number" name="quantity" class="form-control text-center" value="{{$row->qty}}">
                                    <input type="hidden" name="rowId" value="{{$row->rowId}}"/>
                                    <input type="submit" name="update" class="btn btn-info btn-sm" value="Update"/>
                                </form>
                            </td>

                            <td data-th="Subtotal" class="text-center">${{$row->qty * $row->price}}</td>
                            <td class="actions" data-th="">

                                <a href="/cart/remove/{{$row->rowId}}" class="btn btn-danger btn-sm"><i  class="fa fa-trash-o"></i></a>
                            </td>

                    </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                    <tr class="visible-xs">
                            <td class="text-center"><strong>Total( {{Cart::count()}} items) ${{Cart::total()}}</strong></td>
                    </tr>
                    <tr>
                            <td><a href="/category" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                            <td><a href="/cart/clear" class="btn btn-danger"><i class=""></i> Clear Cart</a></td>
                            <td class="text-center"><strong>Subtotal ${{Cart::subtotal()}}</strong></td>
                            <td class="text-center"><strong>Tax ${{Cart::tax()}}</strong></td>
                            <td class="hidden-xs text-center"><strong>Total ${{Cart::total()}}</strong></td>
                            <td>{{Form::open(array('method'=>'post','url'=>'/checkout'))}}<input type="submit" value="Checkout" class="btn btn-success btn-block" />{{Form::close()}}</td>
                    </tr>
                    </tfoot>
            </table>
    <?php else: ?>
    <div class="align_center">
      <img src="/images/empty-cart.jpg" alt="empty cart" /><br /><br/>
      <a href="/category" class="btn btn-warning"> Continue Shopping</a>
    </div>
    <?php endif;?>
    </div>
</div><!-- main-content-->

@endsection
