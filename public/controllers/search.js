/**
 * Controller for Search Bar
 * @author Nigel
 */
app.controller('searchController', function($scope, $http, API_URL){
    $http.get(API_URL + "books")
        .success(function(response) {
            $scope.books = response;
        });
});
