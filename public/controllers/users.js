/**
 * Controller for Users CRUD
 * @author Nigel
 */
app.controller('usersController', function($scope, $http, API_URL){
    $http.get(API_URL + "users")
        .success(function(response) {
            $scope.users = response;
        });

    // Show modal
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.modal_title = "Add New Customer";
                $scope.user = "";
                break;
            case 'edit':
                $scope.modal_title = "Customer Detail";
                $scope.user = "";
                $scope.id = id;
                $http.get(API_URL + 'users/' + id)
                    .success(function(response) {
                        console.log(response);
                        $scope.user = response;
                    });
                break;
            default:
                break;
        }
        console.log(id);
        $('#userModal').modal('show');
    }

    // Save / Update Record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "users";

        if(modalstate === 'edit') {
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.user),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert("An error occured.");
        });
    }

    // Delete Record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'users/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete user');
                    });
        } else {
            return false;
        }
    }
});
