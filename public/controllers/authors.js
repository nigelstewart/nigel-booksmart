/**
 * Controller for Authors CRUD
 * @author Nigel
 */
app.controller('authorsController', function($scope, $http, API_URL){
    $http.get(API_URL + "authors")
        .success(function(response) {
            $scope.authors = response;
        });

    // Show modal
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.modal_title = "Add New Author";
                $scope.author = "";
                break;
            case 'edit':
                $scope.modal_title = "Author Detail";
                $scope.author = "";
                $scope.id = id;
                $http.get(API_URL + 'authors/' + id)
                    .success(function(response) {
                        console.log(response);
                        $scope.author = response;
                    });
                break;
            default:
                break;
        }
        console.log(id);
        $('#authorModal').modal('show');
    }

    // Save / Update Record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "authors";

        if(modalstate === 'edit') {
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.author),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert("An error occured.");
        });
    }

    // Delete Record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'authors/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                        alert('Author deleted with ID ' + id)
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete author');
                    });
        } else {
            return false;
        }
    }
});
