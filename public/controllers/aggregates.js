/**
 * Controller for Aggregate Functions
 * @author Nigel
 */

app.controller('aggregatesController', function($scope, $http, API_URL){
    // Counts
    $http.get(API_URL + "aggregates/count")
        .success(function(response) {
            $scope.count = response;
            authors = $scope.count.authors;
            books = $scope.count.books;
            publishers = $scope.count.publishers;
            orders = $scope.count.orders;
            users = $scope.count.users;
            categories = $scope.count.categories;

            var ctx = document.getElementById("countChart");

            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Authors", "Books", "Publishers", "Orders", "Users", "Categories"],
                    datasets: [{
                        label: 'Items in Database',
                        data: [authors, books, publishers, orders, users, categories],
                        backgroundColor: [
                            'rgb(255, 99, 132)',
                            'rgb(54, 162, 235)',
                            'rgb(255, 206, 86)',
                            'rgb(75, 192, 192)',
                            'rgb(153, 102, 255)',
                            'rgb(104, 177, 79)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(104, 177, 79, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
                    legend: {
                        display:false
                    }
                }
            });
        });

    // Orders totals
    $http.get(API_URL + "aggregates/orders_totals")
        .success(function(response) {
            $scope.orders_totals = response;
            min = $scope.orders_totals.min;
            avg = $scope.orders_totals.avg;
            max = $scope.orders_totals.max;

            var ctx = document.getElementById("ordersTotalsChart");

            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Min $", "Average $", "Max $"],
                    datasets: [{
                        label: 'Orders Totals',
                        data: [min, avg, max],
                        backgroundColor: [
                            'rgb(99, 255, 218)',
                            'rgb(244, 146, 45)',
                            'rgb(88, 155, 189)'
                        ],
                        borderColor: [
                            'rgba(99,255,218,1)',
                            'rgba(244, 146, 45, 1)',
                            'rgba(88, 155, 189, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
                    legend: {
                        display:false
                    }
                }
            });
        });

    // Book Prices
    $http.get(API_URL + "aggregates/book_prices")
        .success(function(response) {
            $scope.book_prices = response;
            min = $scope.book_prices.min;
            avg = $scope.book_prices.avg;
            max = $scope.book_prices.max;

            var ctx = document.getElementById("bookPricesChart");

            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Min $", "Average $", "Max $"],
                    datasets: [{
                        label: 'Book Prices',
                        data: [min, avg, max],
                        backgroundColor: [
                            'rgb(99, 255, 218)',
                            'rgb(244, 146, 45)',
                            'rgb(88, 155, 189)'
                        ],
                        borderColor: [
                            'rgba(99,255,218,1)',
                            'rgba(244, 146, 45, 1)',
                            'rgba(88, 155, 189, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
                    legend: {
                        display:false
                    }
                }
            });
        });

});
