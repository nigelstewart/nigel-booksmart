/**
 * Controller for Categories CRUD
 * @category Nigel
 */
app.controller('categoriesController', function($scope, $http, API_URL){
    $http.get(API_URL + "categories")
        .success(function(response) {
            $scope.categories = response;
        });

    // Show modal
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.modal_title = "Add New Publisher";
                $scope.category = "";
                break;
            case 'edit':
                $scope.modal_title = "Publisher Detail";
                $scope.category = "";
                $scope.id = id;
                $http.get(API_URL + 'categories/' + id)
                    .success(function(response) {
                        console.log(response);
                        $scope.category = response;
                    });
                break;
            default:
                break;
        }
        console.log(id);
        $('#categoryModal').modal('show');
    }

    // Save / Update Record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "categories";

        if(modalstate === 'edit') {
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.category),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert("An error occured.");
        });
    }

    // Delete Record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'categories/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                        alert('Category deleted with ID ' + id)
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete category');
                    });
        } else {
            return false;
        }
    }
});
