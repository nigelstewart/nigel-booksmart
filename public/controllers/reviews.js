/**
 * Controller for Reviews CRUD
 * @author Nigel
 */
app.controller('reviewsController', function($scope, $http, API_URL){
    $http.get(API_URL + "reviews")
        .success(function(response) {
            $scope.reviews = response;
        });

    // Show modal
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.modal_title = "Add New Review";
                $scope.review = "";
                break;
            case 'edit':
                $scope.modal_title = "Review Detail";
                $scope.review = "";
                $scope.id = id;
                $http.get(API_URL + 'reviews/' + id)
                    .success(function(response) {
                        console.log(response);
                        $scope.review = response;
                    });
                break;
            default:
                break;
        }
        console.log(id);
        $('#reviewModal').modal('show');
    }

    // Save / Update Record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "reviews";

        if(modalstate === 'edit') {
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.review),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert("An error occured.");
        });
    }

    // Delete Record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'reviews/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                        alert('Review deleted with ID ' + id)
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete review');
                    });
        } else {
            return false;
        }
    }
});
