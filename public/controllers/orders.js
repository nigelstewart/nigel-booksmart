/**
 * Controller for Orders CRUD
 * @order Nigel
 */
app.controller('ordersController', function($scope, $http, API_URL){
    $http.get(API_URL + "orders")
        .success(function(response) {
            $scope.orders = response;
        });

    // Show modal
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.modal_title = "Add New Order";
                $scope.order = "";
                break;
            case 'edit':
                $scope.modal_title = "Order Detail";
                $scope.order = "";
                $scope.id = id;
                $http.get(API_URL + 'orders/' + id)
                    .success(function(response) {
                        console.log(response);
                        $scope.order = response;
                    });
                break;
            default:
                break;
        }
        console.log(id);
        $('#orderModal').modal('show');
    }

    // Save / Update Record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "orders";

        if(modalstate === 'edit') {
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.order),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert("An error occured.");
        });
    }

    // Delete Record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'orders/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                        alert('Order deleted with ID ' + id)
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete order');
                    });
        } else {
            return false;
        }
    }
});
