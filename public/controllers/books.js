/**
 * Controller for Books CRUD
 * @author Nigel
 */
app.controller('booksController', function($scope, $http, API_URL){
    $http.get(API_URL + "books")
        .success(function(response) {
            $scope.books = response;
        });
    $http.get(API_URL + "authors")
        .success(function(response) {
            $scope.authors = response;
        });
    $http.get(API_URL + "publishers")
        .success(function(response) {
            $scope.publishers = response;
        });
    $http.get(API_URL + "categories")
        .success(function(response) {
            $scope.categories = response;
        });

    // Show modal
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.modal_title = "Add New Book";
                $scope.book = "";
                break;
            case 'edit':
                $scope.modal_title = "Book Detail";
                $scope.book = "";
                $scope.id = id;
                $http.get(API_URL + 'books/' + id)
                    .success(function(response) {
                        console.log(response);
                        $scope.book = response;
                    });
                break;
            default:
                break;
        }
        console.log(id);
        $('#bookModal').modal('show');
    }

    // Save / Update Record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "books";

        if(modalstate === 'edit') {
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.book),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert("An error occured.");
        });
    }

    // Delete Record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'books/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                        alert('Book deleted with ID ' + id)
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete book');
                    });
        } else {
            return false;
        }
    }
});
