/**
 * Controller for Publishers CRUD
 * @publisher Nigel
 */
app.controller('publishersController', function($scope, $http, API_URL){
    $http.get(API_URL + "publishers")
        .success(function(response) {
            $scope.publishers = response;
        });

    // Show modal
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.modal_title = "Add New Publisher";
                $scope.publisher = "";
                break;
            case 'edit':
                $scope.modal_title = "Publisher Detail";
                $scope.publisher = "";
                $scope.id = id;
                $http.get(API_URL + 'publishers/' + id)
                    .success(function(response) {
                        console.log(response);
                        $scope.publisher = response;
                    });
                break;
            default:
                break;
        }
        console.log(id);
        $('#publisherModal').modal('show');
    }

    // Save / Update Record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "publishers";

        if(modalstate === 'edit') {
            url += "/" + id;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.publisher),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            location.reload();
        }).error(function(response) {
            console.log(response);
            alert("An error occured.");
        });
    }

    // Delete Record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'publishers/' + id
            }).
                    success(function(data) {
                        console.log(data);
                        location.reload();
                        alert('Publisher deleted with ID ' + id)
                    }).
                    error(function(data) {
                        console.log(data);
                        alert('Unable to delete publisher');
                    });
        } else {
            return false;
        }
    }
});
