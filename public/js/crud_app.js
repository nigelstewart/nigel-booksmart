angular.module('crudApp', [])

.controller('mainController', function($scope) {

    $scope.sortType = 'id';
    $scope.sortReverse = false;
    $scope.searchTable = '';

    $scope.users = [
        { id: 1, first_name: 'Nigel', last_name: 'Stewart', username: 'nigel@nigelstewart.tech', address: '123 Fake Street', postal_code: 'R2X 9V5', country: 'Canada', phone: '204-999-9999', pasword: 'p@ss0rd' },
        { id: 2, first_name: 'Khushboo', last_name: 'BAtish', username: 'batishkhusboo1994@gmail.com', address: '123 Fake Street', postal_code: 'R2X 9V5', country: 'Canada', phone: '204-999-9999', pasword: 'p@ss0rd'},
        { id: 3, first_name: 'Mohit', last_name: 'Saltora', username: 'Timmisal@hotmail.com', address: '123 Fake Street', postal_code: 'R2X 9V5', country: 'Canada', phone: '204-999-9999', pasword: 'p@ss0rd'},
        { id: 4, first_name: 'Bhumi', last_name: 'Patel', username: 'bpatel.7108@gmail.com', address: '123 Fake Street', postal_code: 'R2X 9V5', country: 'Canada', phone: '204-999-9999', pasword: 'p@ss0rd'},
        { id: 5, first_name: 'Hemali', last_name: 'Patel', username: 'hemali6186@hotmail.com', address: '123 Fake Street', postal_code: 'R2X 9V5', country: 'Canada', phone: '204-999-9999', pasword: 'p@ss0rd'},
        { id: 6, first_name: 'Sanders', last_name: 'Belleveau-Pattern', username: 'SEBP@live.com', address: '123 Fake Street', postal_code: 'R2X 9V5', country: 'Canada', phone: '204-999-9999', pasword: 'p@ss0rd'},
        { id: 7, first_name: 'Parminder', last_name: 'Grewal', username: 'grewalparminder92@gmail.com', address: '123 Fake Street', postal_code: 'R2X 9V5', country: 'Canada', phone: '204-999-9999', pasword: 'p@ss0rd'}
    ];

    $scope.books = [
        { id: 1, title: 'In Search of Lost Time', author: 'Marcel Proust', description: 'Swann\'s Way, the first part of A la recherche de temps perdu, Marcel Proust\'s seven-part cycle, was published in 1913. In it, Proust introduces the themes that run through the entire work. The narrator recalls his childhood, aided by the famous madeleine; and describes M. Swann\'s passion for Odette. The work is incomparable. Edmund Wilson said "[Proust] has supplied for the first time in literature an equivalent in the full scale for the new theory of modern physics."', publish_date: '1913', selling_price: '14.99', cost_price: '8.50', in_stock: '14', num_pages: '200', author_id: '23', publisher_id: '12', category_id: '3' }
    ];

    $scope.orders = [
        { id: 1, customer: 'Bob Loblaw', date: '2016-09-08', subtotal: 23.99, gst: 2.39, pst: 3.98, grand_total: 28.43, credit_card: 8909, status: 'Shipped', address: '123 Fake Street', city: 'Winnipeg', province: 'MB', country: 'Canada', postal_code: 'R2V 1OX'}
    ];

});
