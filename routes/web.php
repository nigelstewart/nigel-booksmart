<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/* Regular Pages */
Route::get('/', 'PagesController@index');

Route::get('/about', 'PagesController@about');

Route::get('/faq', 'PagesController@faq');

Route::get('/terms', 'PagesController@terms');

Route::get('/conditions','PagesController@conditions');

Route::get('/privacy', 'PagesController@privacy');

Route::get('/book_detail/{id}','PagesController@book_detail');

Route::post('/book_detail/{id}', 'ReviewController@store');

Route::get('/author_detail/{id}','PagesController@author_detail');

Route::get('/publisher_detail/{id}','PagesController@publisher_detail');

Route::get('/featured', 'PagesController@featured');

Route::get('/contact', 'PagesController@contact');

Route::post('/contact','ContentController@sendmail');

Route::get('/category/{name}', 'PagesController@category_detail');

Route::get('/thankyou/{id}', 'PagesController@thankyou');

Route::get('/cart/','CartController@view');

Route::get('/cart/add','CartController@add_to_cart');

Route::get('/cart/clear','CartController@destroy');

Route::get('/cart/remove/{rowid}','CartController@remove');

Route::get('/cart/update','CartController@update');

Route::post('/payment','CartController@payment');

Route::post('/checkout','CheckoutController@checkout');

Route::get('/payment/fail', 'CartController@payment_fail');

Route::post('/pay','CheckoutController@process_payment');

/* User profiles */
Route::resource('/profile', 'ProfileController');

Route::post('/profile', 'ProfileController@update');

/* Authentication */
Auth::routes();

Route::get('/home', 'HomeController@index');

/**
 * Admin routes
 * @author Nigel
 */
Route::get('/admin', 'AdminController@aggregates');

// Aggregates Route Group
Route::group(['prefix' => 'admin/aggregates'], function () {
    Route::get('count', 'AggregatesController@count_db_items');
    Route::get('orders_totals', 'AggregatesController@orders_totals');
    Route::get('book_prices', 'AggregatesController@book_prices');
});

// CRUDs Route Group
Route::group(['prefix' => 'admin/panel'], function () {
    Route::get('users', 'AdminController@users_crud');
    Route::get('books', 'AdminController@books_crud');
    Route::get('authors', 'AdminController@authors_crud');
    Route::get('orders', 'AdminController@orders_crud');
    Route::get('publishers', 'AdminController@publishers_crud');
    Route::get('reviews', 'AdminController@reviews_crud');
    Route::get('categories', 'AdminController@categories_crud');
});

Route::get('/admin/users/{id?}', 'Users@index');
Route::post('/admin/users', 'Users@store');
Route::post('/admin/users/{id}', 'Users@update');
Route::delete('/admin/users/{id}', 'Users@destroy');

Route::get('/admin/books/{id?}', 'Books@index');
Route::post('/admin/books', 'Books@store');
Route::post('/admin/books/{id}', 'Books@update');
Route::delete('/admin/books/{id}', 'Books@destroy');

Route::get('/admin/authors/{id?}', 'AuthorController@index');
Route::post('/admin/authors', 'AuthorController@store');
Route::post('/admin/authors/{id}', 'AuthorController@update');
Route::delete('/admin/authors/{id}', 'AuthorController@destroy');

Route::get('/admin/orders/{id?}', 'OrderController@index');
Route::post('/admin/orders', 'OrderController@store');
Route::post('/admin/orders/{id}', 'OrderController@update');
Route::delete('/admin/orders/{id}', 'OrderController@destroy');

Route::get('/admin/publishers/{id?}', 'PublisherController@index');
Route::post('/admin/publishers', 'PublisherController@store');
Route::post('/admin/publishers/{id}', 'PublisherController@update');
Route::delete('/admin/publishers/{id}', 'PublisherController@destroy');

Route::get('/admin/reviews/{id?}', 'ReviewController@index');
Route::post('/admin/reviews', 'ReviewController@store');
Route::post('/admin/reviews/{id}', 'ReviewController@update');
Route::delete('/admin/reviews/{id}', 'ReviewController@destroy');

Route::get('/admin/categories/{id?}', 'CategoryController@index');
Route::post('/admin/categories', 'CategoryController@store');
Route::post('/admin/categories/{id}', 'CategoryController@update');
Route::delete('/admin/categories/{id}', 'CategoryController@destroy');
