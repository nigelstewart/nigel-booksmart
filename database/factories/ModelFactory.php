<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    $name = explode(' ',  $faker->name);

    return [
        'first_name' => $name[0],
        'last_name' => $name[1],
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'province' => $faker->state,
        'country' => $faker->country,
        'postal_code' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'deleted' => 0,
    ];
});
