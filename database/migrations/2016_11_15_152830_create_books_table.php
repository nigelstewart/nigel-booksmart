<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isbn');
            $table->string('title');
            $table->text('description');
            $table->string('publish_date');
            $table->decimal('selling_price', 20, 2);
            $table->decimal('cost_price', 20, 2); //Buying price
            $table->integer('copies_in_stock')->default(100);
            $table->integer('num_pages')->default(150);
            $table->integer('author_id');
            $table->integer('publisher_id');
            $table->integer('category_id');
            $table->string('image');
            $table->timestamps();
            $table->boolean('deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}
