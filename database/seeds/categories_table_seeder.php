<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class categories_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('categories')->insert([
    		'name'=>'Fiction',
            'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
    		]);

       DB::table('categories')->insert([
             'name'=>'Thriller',
             'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
            ]);

       DB::table('categories')->insert([
              'name'=>'Poetry',
              'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
               ]);


     DB::table('categories')->insert([
              'name'=>'Romance',
              'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
              ]);

     DB::table('categories')->insert([
               'name'=>'Horror',
               'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
             ]);

    DB::table('categories')->insert([
              'name'=>'Comics',
              'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
               ]);

    DB::table('categories')->insert([
            'name'=>'Nonfiction',
            'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
             ]);

    DB::table('categories')->insert([
           'name'=>'Kids',
           'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

    DB::table('categories')->insert([
          'name'=>'Religion',
          'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
          ]);

  DB::table('categories')->insert([
    'name'=>'Action',
    'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
    ]);

    }

 }
