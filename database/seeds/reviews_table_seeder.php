<?php

use Illuminate\Database\Seeder;

class reviews_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 150; $i++){
            DB::table('reviews')->insert([
                'book_id' => rand(1,100),
                'user_id' => rand(1,50),
                'stars' => rand(1,5),
                'message' => 'In bibendum risus vitae purus volutpat ultrices. Aliquam finibus mattis nulla, non lacinia metus malesuada non. Curabitur imperdiet vestibulum lectus. Aliquam laoreet fermentum eros, ac dapibus nunc congue at. Integer ut massa eu ante consectetur auctor eget eget felis. Proin accumsan, augue ac aliquet consequat, arcu nisl sagittis metus, in aliquam magna tellus vitae est. Nulla in eros lorem. Mauris eu felis id justo venenatis condimentum. Donec tempor iaculis massa eu ultricies. Nam porta diam urna, id porttitor lorem hendrerit vitae. Nulla facilisis magna in quam mattis eleifend. Integer at posuere lorem. Morbi gravida nisi quam, ut placerat est cursus ac. Nulla sed nisi porttitor, maximus mauris et, viverra urna. Suspendisse eget ante eget elit gravida bibendum.',
                'deleted' => 0
            ]);
        }
    }
}
