<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class authors_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
	   'name'=>'Annie Proulx',
	   'country'=>'USA',
	   'phone'=>'1-847-599-4017 ' ,
	   'email'=>'annieroulx@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);



      DB::table('authors')->insert([
	   'name'=>'Garth Greenwell',
	   'country'=>'USA',
	   'phone'=>'1-847-509-4444' ,
	   'email'=>'Garth_Greenwell@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Sarah Schulman',
	   'country'=>'USA',
	   'phone'=>'1-847-333-4017'  ,
	   'email'=>' sarahschulman@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Peter Ho Davies',
	   'country'=>'Canada',
	   'phone'=>'1-666-777-8888 ' ,
	   'email'=>' peterho@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

      DB::table('authors')->insert([
	   'name'=>'Parisa Reza',
	   'country'=>'Canada',
	   'phone'=>'1-878-599-4017'  ,
	   'email'=>' parisa@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Emma Cline',
	   'country'=>'USA',
	   'phone'=>'1-857-599-4047'  ,
	   'email'=>' emmacline@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Joan London',
	   'country'=>'USA',
	   'phone'=>'1-847-599-4017 ' ,
	   'email'=>' joanlondon@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

      DB::table('authors')->insert([
	   'name'=>'Yaa Gyasi',
	   'country'=>'Usa',
	   'phone'=>'1-847-599-4017'  ,
	   'email'=>'yaagyasi@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Deborah Levy',
	   'country'=>'canada',
	   'phone'=>'2-222-3333-4444'  ,
	   'email'=>' deborah_levy@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Eliot Pattison',
	   'country'=>'canada',
	   'phone'=>'1-847-599-4675'  ,
	   'email'=>' Eliot_Pattison@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

      DB::table('authors')->insert([
	   'name'=>'Bernard Miner',
	   'country'=>'usa',
	   'phone'=>'1-847-556-4017'  ,
	   'email'=>' bernerd@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Anton Svensson',
	   'country'=>'canada',
	   'phone'=>'1-847-566-4017 ' ,
	   'email'=>' antonsvensson@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Timothy Hallinan',
	   'country'=>'canada',
	   'phone'=>'1-847-556-4017'  ,
	   'email'=>'timothyhallinan@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Louise Penny',
	   'country'=>'canada',
	   'phone'=>'1-847-556-4017'  ,
	   'email'=>' louisepenny@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);




      DB::table('authors')->insert([
	   'name'=>'Antonia Hodgson',
	   'country'=>'usa',
	   'phone'=>'1-877-536-4017 ' ,
	   'email'=>' antoniahodgson@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);



      DB::table('authors')->insert([
	   'name'=>'Nicola Upson',
	   'country'=>'usa',
	   'phone'=>'1-837-556-4017'  ,
	   'email'=>'nicolaupson@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);



      DB::table('authors')->insert([
	   'name'=>'John Hart',
	   'country'=>'usa',
	   'phone'=>'1-847-556-4717 ' ,
	   'email'=>' johnhart@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Ben.H.Winters',
	   'country'=>'canada',
	   'phone'=>'1-847-556-4017 ' ,
	   'email'=>' benwinters@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Paul Halter',
	   'country'=>'usa',
	   'phone'=>'1-847-556-4417'  ,
	   'email'=>'paulhalterd@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);



      DB::table('authors')->insert([
	   'name'=>'Fiona Barton',
	   'country'=>'usa',
	   'phone'=>'1-847-556-4087'  ,
	   'email'=>' fionabarton@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Tyehimba jess',
	   'country'=>'usa',
	   'phone'=>'1-847-556-4017'  ,
	   'email'=>'tyehimbajess@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

      DB::table('authors')->insert([
	   'name'=>'C.D.Wright',
	   'country'=>'usa',
	   'phone'=>'1-847-556-4017'  ,
	   'email'=>' wright@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);




      DB::table('authors')->insert([
	   'name'=>'Brenda Shaughnessy',
	   'country'=>'canada',
	   'phone'=>'1-847-566-4017 ' ,
	   'email'=>' brendashaughnessy@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Aracelis Girmay',
	   'country'=>'canada',
	   'phone'=>'1-847-562-4017 ' ,
	   'email'=>' aracelisgirmay@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

      DB::table('authors')->insert([
	   'name'=>'Barbara Ferrer ',
	   'country'=>'usa',
	   'phone'=>'1-847-576-4897 ' ,
	   'email'=>' barbaraerre@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);



      DB::table('authors')->insert([
	   'name'=>'Kelly Bowen',
	   'country'=>'canada',
	   'phone'=>'1-847-566-4327 ' ,
	   'email'=>' kellyoweny@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Erin Finnegan',
	   'country'=>'canada',
	   'phone'=>'1-847-543-4017 ' ,
	   'email'=>' erinfinnegan@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Joanna Shupe',
	   'country'=>'usa',
	   'phone'=>'1-847-563-4017 ' ,
	   'email'=>' joannashupe@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


      DB::table('authors')->insert([
	   'name'=>'Nora Roberts',
	   'country'=>'usa',
	   'phone'=>'1-567-566-4017 ' ,
	   'email'=>' nora@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);


           DB::table('authors')->insert([
	   'name'=>'Jodi Thomas',
	   'country'=>'canada',
	   'phone'=>'1-567-566-4017 ' ,
	   'email'=>' jodithomas@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);



       DB::table('authors')->insert([
	   'name'=>'E L James',
	   'country'=>'canada',
	   'phone'=>'1-567-987-4017 ' ,
	   'email'=>' james@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

       DB::table('authors')->insert([
	   'name'=>'LAURELL K. HAMILTON',
	   'country'=>'usa',
	   'phone'=>'1-567-987-4677 ' ,
	   'email'=>' larell@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

       DB::table('authors')->insert([
	   'name'=>'Nalini Singh',
	   'country'=>'usa',
	   'phone'=>'1-788-987-4017 ' ,
	   'email'=>' nalinisingh@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

       DB::table('authors')->insert([
	   'name'=>'Hilary Mantel',
	   'country'=>'canada',
	   'phone'=>'1-678-900-4017 ' ,
	   'email'=>' hilarymantel@gmail.com',
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
       'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
	   ]);

       DB::table('authors')->insert([
        'name'=>'Grace Burrowes',
	    'country'=>'usa',
	    'phone'=>'1-234-456-3496',
	    'email'=>'grace@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	    DB::table('authors')->insert([
        'name'=>'Robert Jackson Bennett',
	    'country'=>'canada',
	    'phone'=>'1-222-333-5555',
	    'email'=>'robert@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'N.K. Jemisin',
	    'country'=>'china',
	    'phone'=>'2-444-444-5555',
	    'email'=>'jemisin@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Kij Johnson',
	    'country'=>'usa',
	    'phone'=>'3-555-777-5555',
	    'email'=>'johnson@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Ted Kosmatka',
	    'country'=>'canada',
	    'phone'=>'1-111-111-1111',
	    'email'=>'ted@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	      DB::table('authors')->insert([
        'name'=>'Patricia A. McKillip ',
	    'country'=>'Australia',
	    'phone'=>'2-222-222-2222',
	    'email'=>'adc@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Sharon Shinn ',
	    'country'=>'usa',
	    'phone'=>'2-444-555-6666',
	    'email'=>'shinn@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Ann and Jeff Vandermee',
	    'country'=>'canada',
	    'phone'=>'5-555-555-5555',
	    'email'=>'jeff@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Maureen McHugh ',
	    'country'=>'newyork',
	    'phone'=>'7-777-777-7777',
	    'email'=>'hugh@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Lauren Beukes ',
	    'country'=>'usa',
	    'phone'=>'3-333-333-3333',
	    'email'=>'beukes@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'J.M. Frey ',
	    'country'=>'australia ',
	    'phone'=>'6-666-666-6666',
	    'email'=>'frey@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Daryl Gregory ',
	    'country'=>'usa',
	    'phone'=>'0-111-222-3333',
	    'email'=>'daryl@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'A.M. Tuomala ',
	    'country'=>'canada',
	    'phone'=>'4-444-333-4444',
	    'email'=>'am@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Adam Roberts ',
	    'country'=>'canada',
	    'phone'=>'1-333-444-4444',
	    'email'=>'adam@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Mur Lafferty ',
	    'country'=>'canada',
	    'phone'=>'2-333-444-5555',
	    'email'=>'mur@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Darwyn Cooke and Richard Stark
',
	    'country'=>'usa',
	    'phone'=>'4-444-444-4444',
	    'email'=>'cooke@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Lemons
Josh Cotter',
	    'country'=>'australia',
	    'phone'=>'1-222-333-4444',
	    'email'=>'cotter@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Apostolos Doxiadis and Christos H. Papad ',
	    'country'=>'uk',
	    'phone'=>'3-666-777-5555',
	    'email'=>'papad@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Guibert',
	    'country'=>'uk',
	    'phone'=>'4-777-333-5555',
	    'email'=>'gui@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'David Mazzucchelli ',
	    'country'=>'uk',
	    'phone'=>'3-444-666-4444',
	    'email'=>'david@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Bryan Lee OMalley ',
	    'country'=>'uk',
	    'phone'=>'4-222-444-6666',
	    'email'=>'lee@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	     DB::table('authors')->insert([
        'name'=>'Joe Sacco',
	    'country'=>'paris',
	    'phone'=>'1-222-333-5555',
	    'email'=>'joe@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Yoshihiro Tatsumi ',
	    'country'=>'paris',
	    'phone'=>'4-666-666-6666',
	    'email'=>'tauas@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);
	   DB::table('authors')->insert([
        'name'=>'Carol Tyler ',
	    'country'=>'germany',
	    'phone'=>'3-222-888-6666',
	    'email'=>'carol@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Naoki Urasawa ',
	    'country'=>'germany',
	    'phone'=>'8-888-888-8888',
	    'email'=>'naoki@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'David Small ',
	    'country'=>'germany',
	    'phone'=>'5-666-666-6666',
	    'email'=>'david@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'David Grann ',
	    'country'=>'paris',
	    'phone'=>'1-333-222-5555',
	    'email'=>'grann@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Neil Sheehan',
	    'country'=>'uk',
	    'phone'=>'5-555-555-6666',
	    'email'=>'neil@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Richard Holmes ',
	    'country'=>'uk',
	    'phone'=>'8-222-444-6666',
	    'email'=>'holmes@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Frank Bruni ',
	    'country'=>'uk',
	    'phone'=>'5-666-777-5555',
	    'email'=>'bruni@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Cadillac Man',
	    'country'=>'uk',
	    'phone'=>'6-555-777-9999',
	    'email'=>'man@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Dave Cullen ',
	    'country'=>'uk',
	    'phone'=>'9-999-999-9999',
	    'email'=>'cullen@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Barbara Ehrenreich ',
	    'country'=>'uk',
	    'phone'=>'4-666-666-66666',
	    'email'=>'barbara@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'David Finkel',
	    'country'=>'canada',
	    'phone'=>'4-555-666-7777',
	    'email'=>'finkel@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Jaclyn Friedman and Jessica Valenti ',
	    'country'=>'usa',
	    'phone'=>'6-777-888-9999',
	    'email'=>'jaclyn@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Greg Grandin',
	    'country'=>'usa',
	    'phone'=>'9-999-999-9999',
	    'email'=>'greg@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Richard Hamilton',
	    'country'=>'china',
	    'phone'=>'7-999-8888-6666',
	    'email'=>'richard@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Rhoda Janzen ',
	    'country'=>'china',
	    'phone'=>'3-444-555-6666',
	    'email'=>'janzen@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'William Kamkwamba and Bryan Mealer ',
	    'country'=>'china',
	    'phone'=>'2-333-444-5555',
	    'email'=>'william@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Patrick Radden Keefe ',
	    'country'=>'usa',
	    'phone'=>'4-444-444-4444',
	    'email'=>'radden@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Edward M. Kennedy ',
	    'country'=>'canada',
	    'phone'=>'3-333-333-3333',
	    'email'=>'kennedy@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Tracy Kidder ',
	    'country'=>'canada',
	    'phone'=>'4-555-555-6666',
	    'email'=>'kidder@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Tony Persiani',
	    'country'=>'canada',
	    'phone'=>'3-444-555-3333',
	    'email'=>'tony@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Peter Brown ',
	    'country'=>'canada',
	    'phone'=>'9-999-999-8888',
	    'email'=>'brown@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
        'name'=>'Lucy Cousins',
	    'country'=>'china',
	    'phone'=>'3-444-555-6666',
	    'email'=>'lucy@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
 	   ]);

	   DB::table('authors')->insert([
         'name'=>'Chris Gall ',
         'country'=>'usa',
         'phone'=>'4-555-666-7777',
         'email'=>'gall@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'John Hendrix ',
         'country'=>'uk',
         'phone'=>'3-444-565-6767',
         'email'=>'john@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Deborah Hopkinson',
         'country'=>'canada',
         'phone'=>'4-343-232-8989',
         'email'=>'deborah@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Jerry Pinkney',
         'country'=>'china',
         'phone'=>'4-565-676-4545',
         'email'=>'jerry@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Loren Long ',
         'country'=>'usa',
         'phone'=>'2-232-232-4646',
         'email'=>'long@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Lois Lowry',
         'country'=>'uk',
         'phone'=>'2-343-676-8989',
         'email'=>'lois@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Marilyn Nelson ',
         'country'=>'china',
         'phone'=>'3-454-565-7878',
         'email'=>'nelson@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Amy Krouse Rosenthal ',
         'country'=>'uk',
         'phone'=>'3-454-676-5656',
         'email'=>'amy@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Laurie Halse Anderson',
         'country'=>'china',
         'phone'=>'4-555-666-7777',
         'email'=>'halse@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Libba Bray ',
         'country'=>'uk',
         'phone'=>'2-666-787-4545',
         'email'=>'bray@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Kristin Cashore',
         'country'=>'china',
         'phone'=>'2-212-343-5656',
         'email'=>'cashore@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Susan E. Isaacs ',
         'country'=>'canada',
         'phone'=>'5-666-777-8888',
         'email'=>'susan@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Karen Armstrong ',
         'country'=>'canada',
         'phone'=>'4-677-898-4545',
         'email'=>'karen@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Barbara Bradley Hagerty',
         'country'=>'usa',
         'phone'=>'5-666-444-6666',
         'email'=>'bradly@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Harvey Cox',
         'country'=>'canada',
         'phone'=>'4-788-787-4545',
         'email'=>'cox@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Mitch Albom ',
         'country'=>'usa',
         'phone'=>'4-565-676-3434',
         'email'=>'albom@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Paul Wilkes',
         'country'=>'canada',
         'phone'=>'5-676-787-4545',
         'email'=>'paul@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);

        DB::table('authors')->insert([
         'name'=>'Edward E. Curtis IV ',
         'country'=>'germany',
         'phone'=>'5-676-565-5656',
         'email'=>'curtis@gmail.com',
         'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
  	   ]);
    }
}
