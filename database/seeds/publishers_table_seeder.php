<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class publishers_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('publishers')->insert([
      'name'=>'Gabe Habash New-york',
      'country'=>'Newyork',
      'phone'=>'1-204-457-5678',
      'email'=>'Gabbehabash@gmail.com',
      'website'=>'Best-books.publishersweekly.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

     DB::table('publishers')->insert([
      'name'=>'Farrar',
      'country'=>'Newyork',
      'phone'=>'1-204-444-5555',
      'email'=>'Farrar11@gmail.com',
      'website'=>'www.publishersweekly.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

       DB::table('publishers')->insert([
      'name'=>'Feminist',
      'country'=>'Ohio',
      'phone'=>'1-204-555-6666',
      'email'=>'Feminist@gmail.com',
      'website'=>'www.publishersweekly.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

       DB::table('publishers')->insert([
      'name'=>'Houghton Mifflin',
      'country'=>'Canada',
      'phone'=>'2-222-444-5555',
      'email'=>'Houghton.Mifflin@gmail.com',
      'website'=>'www.boks.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

      DB::table('publishers')->insert([
      'name'=>'Adriana Hunter',
      'country'=>'USA',
      'phone'=>'3-444-5555-666',
      'email'=>'Adriana.Hunter@gmail.com',
      'website'=>'www.fiction.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);


     DB::table('publishers')->insert([
      'name'=>'Bill Clegg',
      'country'=>'Canada',
      'phone'=>'3-444-5565-666',
      'email'=>'Bill.Clegg@gmail.com',
      'website'=>'www.books.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);


     DB::table('publishers')->insert([
      'name'=>'Frank',
      'country'=>'Australia',
      'phone'=>'3-666-5565-666',
      'email'=>'frank.23@gmail.com',
      'website'=>'www.fiction.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);


     DB::table('publishers')->insert([
      'name'=>'Knof',
      'country'=>'USA',
      'phone'=>'3-666-5565-666',
      'email'=>'frank.23@gmail.com',
      'website'=>'www.fiction.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

     DB::table('publishers')->insert([
      'name'=>'Bloomsbury',
      'country'=>'USA',
      'phone'=>'3-666-5565-444',
      'email'=>'bloomsbury@gmail.com',
      'website'=>'www.bury.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);


    DB::table('publishers')->insert([
      'name'=>'Natasha Kern',
      'country'=>'Canada',
      'phone'=>'3-666-5455-444',
      'email'=>'natasha@gmail.com',
      'website'=>'www.natasha.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

      DB::table('publishers')->insert([
      'name'=>'Alison Anderson',
      'country'=>'Australia',
      'phone'=>'2-222-444-6666',
      'email'=>'alison@gmail.com',
      'website'=>'www.alison.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

     DB::table('publishers')->insert([
      'name'=>'Elizaeth Clark Wessel',
      'country'=>'Usa',
      'phone'=>'2-545-444-6666',
      'email'=>'elizaeth@gmail.com',
      'website'=>'www.clark.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

         DB::table('publishers')->insert([
      'name'=>'Bob Mecoy',
      'country'=>'Usa',
      'phone'=>'2-545-789-6666',
      'email'=>'bobmecoy@gmail.com',
      'website'=>'www.bob.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);


          DB::table('publishers')->insert([
      'name'=>'Minotaur',
      'country'=>'Australia',
      'phone'=>'2-545-567-6666',
      'email'=>'minotaur@gmail.com',
      'website'=>'www.minotuar.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);



        DB::table('publishers')->insert([
      'name'=>'Mifflin',
      'country'=>'Usa',
      'phone'=>'2-545-567-6785',
      'email'=>'miffin@gmail.com',
      'website'=>'www.miffin.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);


     DB::table('publishers')->insert([
      'name'=>'Grannie',
      'country'=>'Usa',
      'phone'=>'2-545-547-6785',
      'email'=>'grannie@gmail.com',
      'website'=>'www.grannie.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

     DB::table('publishers')->insert([
      'name'=>'Esther Newberg',
      'country'=>'canada',
      'phone'=>'2-596-547-6785',
      'email'=>'esther@gmail.com',
      'website'=>'www.esther.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

      DB::table('publishers')->insert([
      'name'=>'Joelle Delbourgo',
      'country'=>'canada',
      'phone'=>'2-596-547-6785',
      'email'=>'joelle@gmail.com',
      'website'=>'www.joelle.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

      DB::table('publishers')->insert([
      'name'=>'Alan',
      'country'=>'Australia',
      'phone'=>'2-593-547-6785',
      'email'=>'alane@gmail.com',
      'website'=>'www.alan.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('publishers')->insert([
      'name'=>'medeleine Milburn',
      'country'=>'Australia',
      'phone'=>'2-593-547-6785',
      'email'=>'Medeleine@gmail.com',
      'website'=>'www.medeleine.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

     DB::table('publishers')->insert([
      'name'=>'Joplin',
      'country'=>'Australia',
      'phone'=>'2-593-547-6485',
      'email'=>'Joplin@gmail.com',
      'website'=>'www.Joplin.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

        DB::table('publishers')->insert([
      'name'=>'Mariya',
      'country'=>'Usa',
      'phone'=>'2-593-547-6535',
      'email'=>'Mariya@gmail.com',
      'website'=>'www.Mariya.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);



        DB::table('publishers')->insert([
      'name'=>'Copper Canyon Press',
      'country'=>'Usa',
      'phone'=>'2-883-547-6535',
      'email'=>'Copper@gmail.com',
      'website'=>'www.Copper.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);



        DB::table('publishers')->insert([
      'name'=>'BOA Editions Ltd.',
      'country'=>'Usa',
      'phone'=>'2-583-847-6535',
      'email'=>'boa@gmail.com',
      'website'=>'www.boa.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);



      DB::table('publishers')->insert([
      'name'=>'Nancy Yost Literary Agency. ',
      'country'=>'Canada',
      'phone'=>'2-593-847-6535',
      'email'=>'nancy@gmail.com',
      'website'=>'www.Nancy.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

       DB::table('publishers')->insert([
      'name'=>'Stefanie Lieberman, Janklow and Nesbit Associates ',
      'country'=>'Canada',
      'phone'=>'2-593-547-6585',
      'email'=>'stefanie@gmail.com',
      'website'=>'www.Stefanie.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

     DB::table('publishers')->insert([
      'name'=>' Interlude Press ',
      'country'=>'Canada',
      'phone'=>'2-593-547-6535',
      'email'=>'stefanie@gmail.com',
      'website'=>'www.Stefanie.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

      DB::table('publishers')->insert([
      'name'=>' Zebra ',
      'country'=>'Canada',
      'phone'=>'2-456-547-6535',
      'email'=>'zebra@gmail.com',
      'website'=>'www.Zebra.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

      DB::table('publishers')->insert([
      'name'=>'  Amy Berkower, Writers House',
      'country'=>'Canada',
      'phone'=>'2-456-567-6535',
      'email'=>'Amy@gmail.com',
      'website'=>'www.Amy.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

       DB::table('publishers')->insert([
      'name'=>'  HQN Books',
      'country'=>'Canada',
      'phone'=>'2-678-567-6535',
      'email'=>'hqn@gmail.com',
      'website'=>'www.hqn.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

       DB::table('publishers')->insert([
      'name'=>' Knopf Doubleday Publishing Group',
      'country'=>'Canada',
      'phone'=>'2-456-908-6535',
      'email'=>'knopf@gmail.com',
      'website'=>'www.Knopf.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

       DB::table('publishers')->insert([
      'name'=>'  Penguin Publishing Group',
      'country'=>'Canada',
      'phone'=>'2-456-567-6890',
      'email'=>'Penguin@gmail.com',
      'website'=>'www.penguin.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

       DB::table('publishers')->insert([
      'name'=>'Signet',
      'country'=>'Canada',
      'phone'=>'2-908-567-6535',
      'email'=>'Signet@gmail.com',
      'website'=>'www.signet.com',
      'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
      ]);

       DB::table('publishers')->insert([
          'name'=>' Bill Hamilton A.M. Heath',
          'country'=>'Canada',
          'phone'=>'2-850-567-6535',
          'email'=>'Bill@gmail.com',
          'website'=>'www.bill.com',
          'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
          ]);

      DB::table('publishers')->insert([
        'name'=>'Axelrod Literary Agency',
        'country'=>'usa',
        'phone'=>'2-222-333-3434',
        'email'=>'axerlrod@gmail.com',
        'website'=>'www.axelrod.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

           DB::table('publishers')->insert([
        'name'=>'the Knight Agency',
        'country'=>'canada',
        'phone'=>'1-111-222-3333',
        'email'=>'knight@gmail.com',
        'website'=>'www.knight.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Johnson’s magazine',
        'country'=>'usa',
        'phone'=>'2-222-222-2222',
        'email'=>'mb@gmail.com',
        'website'=>'www.magazine.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Gernert Company',
        'country'=>'china',
        'phone'=>'3-444-444-4444',
        'email'=>'ger@gmail.com',
        'website'=>'www.gernert@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Howard Morhaim, Howard Morhaim Literary Agency',
        'country'=>'usa',
        'phone'=>'4-444-444-4444',
        'email'=>'morhaim@gmail.com',
        'website'=>'www.morhaim.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'the Ethan Ellenberg Literary Agency',
        'country'=>'canada',
        'phone'=>'5-555-555-5555',
        'email'=>'ethan@gmail.com',
        'website'=>'www.ethan.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Reprint edition',
        'country'=>'canada',
        'phone'=>'7-676-454-5656',
        'email'=>'reprint@gmail.com',
        'website'=>'www.reprint.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Joseph J. Esposito',
        'country'=>'china',
        'phone'=>'5-444-555-6666',
        'email'=>'joseph@gmail.com',
        'website'=>'www.joseph.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Angry Robot',
        'country'=>'usa',
        'phone'=>'6-777-555-6767',
        'email'=>'robot@gmail.com',
        'website'=>'www.angry.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Dragon Moon Press',
        'country'=>'canada',
        'phone'=>'8-888-888-8888',
        'email'=>'dragon@gmail.com',
        'website'=>'www.dragon.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Fairwood Press',
        'country'=>'usa',
        'phone'=>'3-333-333-4545',
        'email'=>'fairwood@gmail.com',
        'website'=>'www.press.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Candlemark & Gleam ',
        'country'=>'canada',
        'phone'=>'1-233-454-3434',
        'email'=>'cand@gmail.com',
        'website'=>'www.candlemark.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Gollancz',
        'country'=>'usa',
        'phone'=>'4-555-555-6666',
        'email'=>'gollancz@gmail.com',
        'website'=>'www.gollancz.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'trade paper ',
        'country'=>'canada',
        'phone'=>'3-555-555-5555',
        'email'=>'paper@gmail.com',
        'website'=>'www.trade.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'DW Publishing',
        'country'=>'usa',
        'phone'=>'4-566-676-6767',
        'email'=>'dw@gmail.com',
        'website'=>'www.publishing.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'AdHouse',
        'country'=>'usa',
        'phone'=>'1-222-333-4444',
        'email'=>'ad@gmail.com',
        'website'=>'www.aduse.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Bloomsbury',
        'country'=>'china',
        'phone'=>'3-333-333-4444',
        'email'=>'bloomsbury@gmail.com',
        'website'=>'www.bloomsbury.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Dupuis',
        'country'=>'usa',
        'phone'=>'2-222-222-3434',
        'email'=>'dupies@gmail.com',
        'website'=>'www.dupuis.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Rubber Blanket',
        'country'=>'canada',
        'phone'=>'1-222-333-4444',
        'email'=>'rubber@gmail.com',
        'website'=>'www.rubber.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Oni Press',
        'country'=>'paris',
        'phone'=>'4-566-677-8989',
        'email'=>'oni@gmail.com',
        'website'=>'www.oni.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Jonathan Cape',
        'country'=>'germany',
        'phone'=>'4-567-678-4556',
        'email'=>'jonathan@gmail.com',
        'website'=>'www.jonathan.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Self-Publishing Companies',
        'country'=>'paris',
        'phone'=>'4-565-446-3445',
        'email'=>'gh@gmail.com',
        'website'=>'www.publishing.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Fantagraphics Books',
        'country'=>'china',
        'phone'=>'3-567-678-3456',
        'email'=>'fan@gmail.com',
        'website'=>'www.Fantagraphics.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Pluto Press ',
        'country'=>'usa',
        'phone'=>'2-456-345-6789',
        'email'=>'pluto@gmail.com',
        'website'=>'www.plutopress.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'W. W. Norton',
        'country'=>'canada',
        'phone'=>'3-555-677-8789',
        'email'=>'norton@gmail.com',
        'website'=>'www.norton.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Knopf Doubleday Publishing Group',
        'country'=>'usa',
        'phone'=>'2-333-444-5667',
        'email'=>'knopf@gmail.com',
        'website'=>'www.knopf.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>' Doubleday',
        'country'=>'paris',
        'phone'=>'4-555-678-4567',
        'email'=>'double@gmail.com',
        'website'=>'www.double.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Random House',
        'country'=>'canada',
        'phone'=>'1-234-456-6789',
        'email'=>'random@gmail.com',
        'website'=>'www.random.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>' HarperPress',
        'country'=>'paris',
        'phone'=>'1-267-789-4567',
        'email'=>'harper@gmail.com',
        'website'=>'www.harperpress.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Penguin Press',
        'country'=>'china',
        'phone'=>'1-234-567-7897',
        'email'=>'penguin@gmail.com',
        'website'=>'www.penguin.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Bloomsbury Publishing PLC ',
        'country'=>'usa',
        'phone'=>'1-345-678-6789',
        'email'=>'bloomsbury@gmail.com',
        'website'=>'www.bloomsbury.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Hachette',
        'country'=>'canada',
        'phone'=>'1-234-567-5678',
        'email'=>'hachette@gmail.com',
        'website'=>'www.hachette.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Picador',
        'country'=>'germany',
        'phone'=>'1-234-567-2345',
        'email'=>'picador@gmail.com',
        'website'=>'www.picador.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Crichton',
        'country'=>'germany',
        'phone'=>'1-345-567-6789',
        'email'=>'crichton@gmail.com',
        'website'=>'www.crichton.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Seal Press',
        'country'=>'uk',
        'phone'=>'1-345-678-2345',
        'email'=>'seal@gmail.com',
        'website'=>'www.sealpress.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>' Actar',
        'country'=>'uk',
        'phone'=>'2-456-678-3456',
        'email'=>'actar@gmail.com',
        'website'=>'www.actar.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Henry Holt and Co',
        'country'=>'paris',
        'phone'=>'1-234-567-3456',
        'email'=>'henry@gmail.com',
        'website'=>'www.henryholt.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Wlliam Morrow & Company ',
        'country'=>'uk',
        'phone'=>'1-567-789-4567',
        'email'=>'willam@gmail.com',
        'website'=>'www.willam.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Poisoned Pen Press',
        'country'=>'australia',
        'phone'=>'1-987-567-3456',
        'email'=>'poisoned@gmail.com',
        'website'=>'www.poisonedpress.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Twelve 1st Edition ',
        'country'=>'australia',
        'phone'=>'2-879-567-459',
        'email'=>'twelve@gmail.com',
        'website'=>'www.press.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Random House',
        'country'=>'canada',
        'phone'=>'1-345-678-2345',
        'email'=>'random@gmail.com',
        'website'=>'www.randomhouse.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Charlesbridge; New edition',
        'country'=>'usa',
        'phone'=>'1-345-678-2345',
        'email'=>'char@gmail.com',
        'website'=>'www.Charlesbridge.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'The Curious Garden',
        'country'=>'germany',
        'phone'=>'2-345-567-2345',
        'email'=>'curious@gmail.com',
        'website'=>'www.curious.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Word Alive',
        'country'=>'germany',
        'phone'=>'1-345-678-7896',
        'email'=>'word@gmail.com',
        'website'=>'www.alive.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Candlewick',
        'country'=>'usa',
        'phone'=>'1-456-567-2011',
        'email'=>'candlemark@gmail.com',
        'website'=>'www.candlemark.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>' Little, Brown Books for Young Readers',
        'country'=>'uk',
        'phone'=>'1-456-456-6890',
        'email'=>'brown@gmail.com',
        'website'=>'www.brownbooks@gmail.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>' customer magazine',
        'country'=>'usa',
        'phone'=>'6-456-789-4589',
        'email'=>'customer@gmail.com',
        'website'=>'www.publicmagazine.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Hyperion Book CH',
        'country'=>'australia',
        'phone'=>'1-234-567-9876',
        'email'=>'Hyperion@gmail.com',
        'website'=>'www.Hyperionbook.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Little, Brown Books for Young Readers',
        'country'=>'uk',
        'phone'=>'2-456-567-8769',
        'email'=>'little@gmail.com',
        'website'=>'www.brown.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Harrison Gray Otis House',
        'country'=>'australia',
        'phone'=>'3-567-435-6785',
        'email'=>'harrrison@gmail.com',
        'website'=>'www.harrisongray.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Scholastic Press',
        'country'=>'china',
        'phone'=>'3-567-456-6785',
        'email'=>'Scholastic@gmail.com',
        'website'=>'www.Scholasticpress.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Dial Books',
        'country'=>'germany',
        'phone'=>'3-678-345-6789',
        'email'=>'dialbook@gmail.com',
        'website'=>'www.dialbooks.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Chronicle Books LLC',
        'country'=>'usa',
        'phone'=>'4-678-456-8945',
        'email'=>'Chronicle@gmail.com',
        'website'=>'www.Chronicle.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>' by Speak',
        'country'=>'usa',
        'phone'=>'1-234-567-8345',
        'email'=>'asghn@gmail.com',
        'website'=>'www.books.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Delacorte Press',
        'country'=>'australia',
        'phone'=>'6-567-345-7123',
        'email'=>'Delacorte@gmail.com',
        'website'=>'wwww.Delacortepress.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'African-American literary magazine',
        'country'=>'paris',
        'phone'=>'2-456-678-9456',
        'email'=>'literary@gmail.com',
        'website'=>'www.aaliterarymagazine.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'FaithWords',
        'country'=>'germany',
        'phone'=>'4-456-678-3478',
        'email'=>'faithwords@gmail.com',
        'website'=>'www.faithwords.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Knopf Publishing Group ',
        'country'=>'uk',
        'phone'=>'3-678-345-4567',
        'email'=>'knopf@gmail.com',
        'website'=>'www.knopfpublishing.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'AV Magazine',
        'country'=>'germany',
        'phone'=>'4-564-563-4567',
        'email'=>'avmagazine@gmail.com',
        'website'=>'www.avmagazine.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'HarperOne',
        'country'=>'germany',
        'phone'=>'4-678-456-3897',
        'email'=>'harperone@gmail.com',
        'website'=>'www.harperone.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);



            DB::table('publishers')->insert([
        'name'=>'Jossey-Bass',
        'country'=>'germany',
        'phone'=>'4-567-234-5678',
        'email'=>'josseybass@gmail.com',
        'website'=>'www.josseybass.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);

            DB::table('publishers')->insert([
        'name'=>'Oxford University',
        'country'=>'usa',
        'phone'=>'1-456-345-6789',
        'email'=>'oxforduniversity@gmail.com',
        'website'=>'www.oxforduniversity.com',
        'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
