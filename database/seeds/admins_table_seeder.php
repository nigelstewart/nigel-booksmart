<?php

use Illuminate\Database\Seeder;

class admins_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @author Mohit Salhotra
     */
    public function run()
    {
        DB::table('admins')->insert(
                [
                    'user_id' => 1
                ]
        );
    }
}
