<?php

use Illuminate\Database\Seeder;

class admin_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'id' => 1,
                'first_name' => 'Mohit',
                'last_name' => 'Salhotra',
                'address' => 'xyz Avenue',
                'city' => 'Winnipeg',
                'province' => 'Manitoba',
                'country' => 'Canada',
                'postal_code' => 'R2R 8W9',
                'phone' => '204-222-2222',
                'email' => 'admin@booksmart.com',
                'password' => bcrypt('mypass'),
                'remember_token' => str_random(10),
            ]

        );
    }
}
