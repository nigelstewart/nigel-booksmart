<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(books_table_seeder::class);
        $this->call(publishers_table_seeder::class);
        $this->call(authors_table_seeder::class);
        $this->call(categories_table_seeder::class);
        $this->call(reviews_table_seeder::class);
        $this->call(admins_table_seeder::class);
        $this->call(admin_seeder::class);
        $this->call(orders_table_seeder::class);

        factory(App\User::class, 50)->create()->each(function($u) {
            //$u->posts()->save(factory(App\Post::class)->make());
        });

    }
}
