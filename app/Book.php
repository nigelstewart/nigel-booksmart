<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * Author: Mohit Salhotra
 * Updated By: Sanders Belliveau-Pattern
 */
class Book extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     protected $fillable = [
        'id',
        'isbn',
        'title',
        'description',
        'publish_date',
        'selling_price',
        'cost_price',
        'copies_in_stock',
        'num_pages',
        'author_id',
        'publisher_id',
        'category_id',
        'image',
        'updated_at'
    ];

    public function author()
    {
        return $this->hasOne('App\Author');
    }

}
