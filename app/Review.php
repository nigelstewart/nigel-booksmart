<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function getUser()
    {
        return $this->belongsTo('App\User');
    }
}
