<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    public function books()
    {
        return $this->hasMany("App\Book");
    }

    /**
     * Created By: Sanders
     * returns the books the author has written
     * @return mixed
     */
    public function authorBooks()
    {
        return $this->hasMany('App\Book')
                    ->join('authors', 'authors.id', '=', 'books.author_id')
                    ->select('books.*', 'authors.*')
                    ->get();
    }

}
