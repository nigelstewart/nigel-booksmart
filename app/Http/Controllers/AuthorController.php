<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Author;

class AuthorController extends Controller
{

    /**
    * Display all authors
    * @author: Nigel
    */
    public function index($id = null) {
        if ($id == null) {
            return Author::orderBy('id', 'asc')
            ->where('deleted', '=', '0')->get();
        } else {
            return $this->show($id);
        }//end if
    }//function index

    /**
     * Display the specified author
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function show($id) {
        return Author::find($id);
    }

    /**
     * Store new author
     * @param Request $request
     * @return response
     * @author Nigel
     */
    public function store(Request $request)
    {
        $author = new Author;

        $author->name = $request->input('name');
        $author->country = $request->input('country');
        $author->phone = $request->input('phone');
        $author->email = $request->input('email');
        $author->deleted = 0;
        $author->save();

        return 'Author record created with id ' . $author->id;
    }

    /**
     * Update existing author
     * @param Request $request
     * @return response
     * @author Nigel
     */
    public function update(Request $request, $id)
    {
        $author = Author::find($id);

        $author->name = $request->input('name');
        $author->country = $request->input('country');
        $author->phone = $request->input('phone');
        $author->email = $request->input('email');
        $author->deleted = 0;
        $author->save();

        return 'Author record updated with id ' . $author->id;
    }

    /**
     * Remove the specified author
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function destroy(Request $request, $id)
    {
        $author = Author::find($id);
        $author->deleted = 1;
        $author->save();

        return 'Author successfully deleted with id #' . $request->input('id');
    }

}
