<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Users extends Controller
{

    /**
     * Display a listing of users.
     * @return Response
     * @author Nigel
     */
    public function index($id = null)
    {
        if ($id == null) {
            return User::orderBy('id', 'asc')->where('deleted', '=', 0)->get();
        } else {
            return $this->show($id);
        }
    }


    /**
     * Store new user
     * @param Request $request
     * @return response
     * @author Nigel
     */
    public function store(Request $request)
    {
        $user = new User;

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->address = $request->input('address');
        $user->city = $request->input('city');
        $user->province = $request->input('province');
        $user->country = $request->input('country');
        $user->postal_code = $request->input('postal_code');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->deleted = 0;
        $user->save();

        return 'User record created with id ' . $user->id;
    }


    /**
     * Display the specified user
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function show($id) {
        return User::find($id);
    }


    /**
     * Update the specified user
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->address = $request->input('address');
        $user->city = $request->input('city');
        $user->province = $request->input('province');
        $user->country = $request->input('country');
        $user->postal_code = $request->input('postal_code');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return 'User record updated with id ' . $user->id;
    }

    /**
     * Remove the specified user
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $user->deleted = 1;
        $user->save();

        return 'User successfully deleted with id #' . $request->input('id');
    }



}
