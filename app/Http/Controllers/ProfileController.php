<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Category;
/**
 * created on nov 23,2016
 * Created by khushboo
 */
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * Author Khushboo
     * @return Response
     */
    public function index()

    {
        $title = "User Profile";
        $categories = Category::Orderby('id', 'asc')->get();
        if (!Auth::user()){
            die('you are not authorized');
        }
        // get all the users
        $users = Auth::user();

        // load the view and pass the users
        return view('users.index',compact('users', 'title', 'categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * Author Khushboo
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *Author Khushboo
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request,$id)
       {
        $categories = Category::Orderby('id', 'asc')->get();
        // get the user
         if (!Auth::user()){
            die('you are not authorized');
        }
        $title="Edit your profile";
        // get all the users
        $users = \App\User::find($id);
        // show the edit form and pass the

        return view('users.profile',compact('users','title','categories'));

    }

    /**
     * Update the specified resource in storage.
     * Author Khushboo
     * @param  int  $id
     * @return Response
     */
       public function update(Request $request)
    {
           if (!Auth::user()){
            die('you are not authorized');
        }
        $user =Auth::user();
       // return dd($user);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->address = $request->input('address');
        $user->city = $request->input('city');
        $user->province = $request->input('province');
        $user->country = $request->input('country');
        $user->postal_code = $request->input('postal_code');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return 'User Profile was updated with id ' . $user->id;
    }


    /**
     * Remove the specified resource from storage.
     *Author khushboo
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
