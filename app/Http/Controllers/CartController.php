<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Book;

use Cart;

use App\Order;

use App\Category;

use Auth;


    /**Cart
     * @author: Bhumi
     */

class CartController extends Controller
{

    /**
     * Adds Item to cart
     * @param Request $request Laravel Voodoo
     */
    public function add_to_cart(Request $request)
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Booksmart Cart Page";
        $quantity = $request->input('quantity');
        $id = $request->input('id');
        $book = Book::find(intval($id));
        Cart::add(['id' => $id, 'name'=> $book['title'], 'qty' => $quantity, 'price'=> $book['selling_price'], 'options' =>['description'=>$book['description']]]);
        $subtotal = Cart::subtotal();
        $count = Cart::count();
        $total = Cart::total();
        return redirect('/cart');
    }

    /**
     * Destroys the cart
     * @return mixed view
     */
    public function destroy()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Destroyed Cart";
        Cart::destroy();
        return view('shoppingcart/cart',compact('title', 'categories'));
    }

    /**
     * Removes a single item
     * @param  integer $rowid the specific item
     * @return redirect
     */
    public function remove($rowid)
    {
        Cart::remove($rowid);
        return redirect('/cart' );

    }

    /**
     * Updates the cart
     * @param  Request $request Laravel Smoke and Mirrors
     * @return Redirect
     */
    public function update(Request $request)
    {
        $title = "Update Cart";
        $rowId = $request->input('rowId');
        $quantity = $request->input('quantity');
        Cart::update($rowId, $quantity);
        return redirect('/cart');

    }

    /**
     * Shows the cart
     * @return mixed view
     */
    public function view()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title="Cart";
        return view('shoppingcart/cart',compact('title', 'categories'));
    }

    /**
     * Inputs an order
     * @param  Request $request Laravel Crazyness
     * @return mixed view
     */
    public function payment(Request $request)
    {
        $categories = Category::Orderby('id', 'asc')->get();
         $this->validate($request,[
            'shippingaddress' => 'required|max:255',
            'shippingcity'=>'required',
            'shippingstate'=>'required',
            'shippingcountry'=>'required',
            'ship_postcode'=>'required',
            'ship_first_name'=>'required',
            'ship_last_name'=>'required',
            'billingaddress' => 'required|max:255',
            'billingcountry'=>'required',
            'billingprovince'=>'required',
            'billingcity'=>'required',
            'billingpostcode'=>'required',
            'bill_first_name'=>'required',
            'bill_last_name'=>'required',
        ]);
        $order = new Order;
        $order->shipping_first_name = filter_var($request->input('ship_first_name'), FILTER_SANITIZE_STRING);
        $order->shipping_last_name = filter_var($request->input('ship_last_name'), FILTER_SANITIZE_STRING);
        $order->shipping_address = filter_var($request->input('shippingaddress'), FILTER_SANITIZE_STRING);
        $order->shipping_country = filter_var($request->input('shippingcountry'), FILTER_SANITIZE_STRING);
        $order->shipping_city = filter_var($request->input('shippingcity'), FILTER_SANITIZE_STRING);
        $order->shipping_province = filter_var($request->input('shippingstate'), FILTER_SANITIZE_STRING);
        $order->shipping_postal_code = filter_var($request->input('ship_postcode'), FILTER_SANITIZE_STRING);
        $order->billing_first_name = filter_var($request->input('bill_first_name'), FILTER_SANITIZE_STRING);
        $order->billing_last_name = filter_var($request->input('bill_last_name'), FILTER_SANITIZE_STRING);
        $order->billing_address = filter_var($request->input('billingaddress'), FILTER_SANITIZE_STRING);
        $order->billing_country = filter_var($request->input('billingcountry'), FILTER_SANITIZE_STRING);
        $order->billing_city = filter_var($request->input('billingcity'), FILTER_SANITIZE_STRING);
        $order->billing_province = filter_var($request->input('billingprovince'), FILTER_SANITIZE_STRING);
        $order->billing_postal_code = filter_var($request->input('billingpostcode'), FILTER_SANITIZE_STRING);
        $order->total = Cart::subtotal() +  number_format(Cart::subtotal() * 0.08) + number_format(Cart::subtotal() * 0.05);
        $order->sub_total = Cart::subtotal();
        $order->gst = number_format(Cart::subtotal() * 0.08);
        $order->pst = number_format(Cart::subtotal() * 0.05);
        $order->order_date = date('Y-m-d h:i:s');
        $order->user_id = Auth::user()->id;

        if($order->save()) {
            $title = "Payment Details";
            return view('shoppingcart/payment',compact('title', 'categories'));
        }
        else {
            return 'There was a problem saving the post';
        }
    }

    /**
     * Checkout page
     * @return mixed view
     */
    public function checkout()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Checkout";
        return view('shoppingcart/checkout',compact('title', 'categories'));
    }

    /**
     * User is Broke
     * @return mixed view
     */
    public function payment_fail()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Books Mart - Payment Page";
        return view('shoppingcart/payment', compact('title', 'categories'));
    }
}
