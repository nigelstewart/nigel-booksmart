<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    /**
     * Get all orders
     * @return Response
     * @author Nigel
     */
    public function index($id = null)
    {
        if ($id == null) {
            return Order::orderBy('id', 'asc')
            ->where('orders.deleted', '=', 0)
            ->get();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Display the specified order
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function show($id) {
        return Order::orderBy('id', 'asc')
        ->where('orders.deleted', '=', 0)
        ->find($id);
    }

    /**
     * Update the specified order
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        $order->shipment_status = $request->input('shipment_status');
        $order->shipping_address = $request->input('shipping_address');
        $order->city = $request->input('city');
        $order->province = $request->input('province');
        $order->country = $request->input('country');
        $order->postal_code = $request->input('postal_code');
        $order->save();

        return 'Order details updated with id ' . $order->id;
    }

    /**
     * Remove the specified order
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function destroy(Request $request, $id)
    {
        $order = Order::find($id);
        $order->deleted = 1;
        $order->save();

        return 'Order successfully deleted with id #' . $request->input('id');
    }
}
