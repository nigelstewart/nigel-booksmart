<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;

class ReviewController extends Controller
{

    /**
     * Get all reviews
     * @return Response
     * @author Nigel
     */
    public function index($id = null)
    {
        if ($id == null) {
            return Review::orderBy('id', 'asc')
            ->where('reviews.deleted', '=', 0)
            ->join('books', 'reviews.book_id', '=', 'books.id')
            ->join('users', 'reviews.user_id', '=', 'users.id')
            ->select('reviews.*', 'users.email', 'books.title as book_title')
            ->get();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Display the specified review
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function show($id) {
        return Review::join('books', 'reviews.book_id', '=', 'books.id')
        ->join('users', 'reviews.user_id', '=', 'users.id')
        ->select('reviews.*', 'users.email', 'books.title as book_title')
        ->find($id);
    }

    /**
     * Update the specified review
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function update(Request $request, $id)
    {
        $review = Review::find($id);

        $review->message = $request->input('message');
        $review->save();

        return 'Review updated with id ' . $review->id;
    }

    /**
     * Remove the specified review
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function destroy(Request $request, $id)
    {
        $review = Review::find($id);
        $review->deleted = 1;
        $review->save();

        return 'Review successfully deleted with id #' . $request->input('id');
    }

    public function store(Request $request)
    {
        $review = new Review;

        $review->book_id = $request->input('book_id');
        $review->user_id = $request->input('user_id');
        $review->stars = $request->input('stars');
        $review->message = $request->input('message');
        $review->deleted = 0;
        $review->save();

        $request->session()->flash('flash-message', 'Review was successfully submitted!');

        return redirect()->back();
    }

}
