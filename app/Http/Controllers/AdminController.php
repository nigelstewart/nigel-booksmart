<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* Controller for admin pages
*@author: Nigel
*/
class AdminController extends Controller
{
    /**
     * Constructor Function
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Index Page
     * @return mixed view
     */
    public function index()
    {
        $title = "Admin Log In";
        return view('admin/index', compact('title'));
    }

    /**
     * Aggregates
     * @return mixed view
     */
    public function aggregates()
    {
        $title = "Site Statistics";
        return view('admin/aggregates', compact('title'));
    }

    /**
     * Users Crud
     * @return mixed view
     */
    public function users_crud()
    {
        $title = "Users";
        return view('admin/users', compact('title'));
    }

    /**
     * Books Crud
     * @return mixed view
     */
    public function books_crud()
    {
        $title = "Books";
        return view('admin/books', compact('title'));
    }

    /**
     * Author Crud
     * @return mixed view
     */
    public function authors_crud()
    {
        $title = "Authors";
        return view('admin/authors', compact('title'));
    }

    /**
     * Orders Crud
     * @return mixed view
     */
    public function orders_crud()
    {
        $title = "Orders";
        return view('admin/orders', compact('title'));
    }

    /**
     * Publishers Crud
     * @return mixed view
     */
    public function publishers_crud()
    {
        $title = "Publishers";
        return view('admin/publishers', compact('title'));
    }

    /**
     * Reviews Crud
     * @return mixed view
     */
    public function reviews_crud()
    {
        $title = "Reviews";
        return view('admin/reviews', compact('title'));
    }

    /**
     * Categories Crud
     * @return mixed view
     */
    public function categories_crud()
    {
        $title = "Categories";
        return view('admin/categories', compact('title'));
    }
}
