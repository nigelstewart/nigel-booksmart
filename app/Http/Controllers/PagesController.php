<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Author;
use App\Publisher;
use App\Book;
use App\Category;
use App\Review;
use Auth;
use Cart;
use App\Order;
use App\Transaction;

class PagesController extends Controller
{
    /**
     * Home page
     * @return mixed view
     */
    public function index()
    {
        $allbooks = array();
        $categories = Category::Orderby('id', 'asc')->get();
        foreach ($categories as $cat) {
           $cat_books = $cat->books()->get();
           $allbooks[$cat->name]=$cat_books;
        }
        $title = "Booksmart Home";
        return view('pages/index', compact('title','allbooks', 'categories') );
    }

    /**
     * About page
     * @author Sanders
     * @return mixed view
     */
    public function about()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "About";
        return view('pages/about', compact('title', 'categories') );
    }

    /**
     * Frequently Asked Questions
     * @author Hemali
     * @return mixed view
     */
    public function faq()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Frequently Asked Questions";
        return view('pages/faq', compact('title', 'categories') );
    }

    /**
     * Terms and Conditions
     * @author Hemali
     * @return mixed view
     */
    public function terms()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Terms and Conditions";
        return view('pages/terms', compact('title', 'categories') );
    }

    /**
     * Conditions of use
     * @author Hemali
     * @return mixed view
     */
    public function conditions()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Conditions of Use";
        return view('pages/terms', compact('title', 'categories') );
    }

    /**
     * Privacy terms
     * @author Hemali
     * @return mixed view
     */
    public function privacy()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Privacy";
        return view('pages/privacy', compact('title', 'categories') );
    }

    /**
     * Contact page
     * @return mixed view
     */
    public function contact()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Contact Us";
        return view('pages/contact', compact('title', 'categories') );
    }

    /**
     * Featured books page
     * @author Sanders
     * @return mixed view
     */
    public function featured()
    {
        $books = Book::all()->random(25);
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Featured";
        return view('pages/featured', compact('title', 'books', 'categories') );
    }

    /**
     * Book Details page
     * @author Sander
     * @param  integer  $id      The Books id
     * @param  Request $request laravel request thingy
     * @return mixed            view
     */
    public function book_detail($id, Request $request)
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $book = Book::join('authors', 'books.author_id', '=', 'authors.id')
                ->join('publishers', 'books.publisher_id', '=', 'publishers.id')
                ->select('books.*', 'authors.name as auth_name', 'publishers.name as pub_name')
                ->find(intval($id));
        $review = Review::where('book_id', '=', "$id")
                        ->join('users', 'reviews.user_id', '=', 'users.id')
                        ->get();
        $title = "Book Detail";
        return view('pages/book_detail',compact('title', 'book', 'review', 'categories'));
    }

    /**
     * Author Details page
     * @author Sanders
     * @param  integer  $id      Authors ID
     * @param  Request $request Laravel Magic
     * @return mixed           views
     */
    public function author_detail($id, Request $request)
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $author = Author::find(intval($id));
        $books = $author->authorBooks();
        $title = "Author Detail";
        return view('pages/author_detail', compact('title','author','books', 'categories') );
    }

    /**
     * Publisher Detail page
     * @author Sanders
     * @param  integer  $id      Publishers ID
     * @param  Request $request Laravel Abra Cadabra
     * @return mixed           view
     */
    public function publisher_detail($id, Request $request)
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $publisher = Publisher::find(intval($id));
        $books = $publisher->publisherBooks();
        $title = "Publisher Detail";
        return view('pages/publisher_detail',compact('title','publisher','books', 'categories'));
    }

    /**
     * Send mail Page, sends the users message
     * @return mixed view
     */
    public function sendmail()
    {
        $title = "Send Mail";
        return view('pages/sendmail',compact('title'));
    }

    /**
     * Book Category Page
     * @return mixed view
     */
    public function book_category()
   {
       $categories = Category::Orderby('id', 'asc')->get();
       $books = Book::Orderby('publish_date')->paginate();
       $title = "Booksmart Home";
       return view('pages/index', compact('title','books', 'categories') );
   }

   /**
    * Category Details
    * @param  string $name name of the category
    * @return mixed       view
    */
   public function category_detail($name)
   {
       $categories = Category::Orderby('id', 'asc')->get();
       $cat = Category::where('name', $name)->first(); // get category where name = |$name'
       $books = Book::where('books.category_id', "=", $cat->id)->orderby('publish_date')->paginate();
       $title = 'Category: ' . $name;
       return view('pages/category_detail',compact('title', 'books', 'categories'));

   }

   /**
    * Thanks you page, after the checkout
    * @param  integer $id The order ID
    * @return mixed     view
    */
   public function thankyou($id)
   {
        $title = "Books Mart Thank you for shopping";
        $transaction  = Transaction::find($id);
        $decoded_object = json_decode($transaction);
        $cart_temp = Cart::content();
        $transaction_status = json_decode($decoded_object->transaction_status);
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Books Mart Thankyou Page";
        $user = Auth::user();
        $order = Order::get()->last();
        Cart::destroy();
        return view('pages/thankyou',compact('title','decoded_object','transaction_status','cart_temp','order','categories'));
    }

}
