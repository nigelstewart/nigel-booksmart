<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ContentController extends Controller
{
    /* Created By: Parminder */

    /**
     * Sends the users mail
     * @param  Request $request
     * @return mixed
     */
    public function sendmail(Request $request)
    {

        // get the form variables
        $data['user'] = filter_var($request->user, FILTER_SANITIZE_STRING);
        $data['email'] = filter_var($request->email, FILTER_SANITIZE_EMAIL);
        $data['phone'] = filter_var($request->phone, FILTER_SANITIZE_NUMBER_INT);
        $data['comment'] = filter_var($request->comment, FILTER_SANITIZE_STRING);
        $data['to_email'] = 'parmindersingh.pcte@gmail.com';
        $data['to_name'] = 'Parminder Grewal';
        dd($data);

        Mail::send('emails.contact', compact('data'), function ($mail) use ($data) {
            $mail->from($data['email'], $data['user'], $data['phone'], $data['comment']);
            $mail->to($data['to_email'], $data['to_name'])->subject('Message from contact form');
        });
    }

}
