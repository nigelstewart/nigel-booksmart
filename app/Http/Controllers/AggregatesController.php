<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Author;
use App\Book;
use App\Publisher;
use App\Order;
use App\User;
use App\Category;

class AggregatesController extends Controller
{
    /**
    * Count items in database
    * @author: Nigel
    */
    public function count_db_items()
    {
        $counts['authors'] = Author::where('deleted', '=', '0')->count();
        $counts['books'] = Book::where('deleted', '=', '0')->count();
        $counts['publishers'] = Publisher::where('deleted', '=', '0')->count();
        $counts['orders'] = Order::where('deleted', '=', '0')->count();
        $counts['users'] = User::where('deleted', '=', '0')->count();
        $counts['categories'] = Category::where('deleted', '=', '0')->count();

        return $counts;
    }


    /**
     * Orders min, average, max
     * @author: Nigel
     */
    public function orders_totals()
    {
        $orders['min'] = Order::min('total');
        $orders['avg'] = Order::avg('total');
        $orders['max'] = Order::max('total');

        return $orders;
    }

    /**
     * Book prices
     * @author: Nigel
     */
    public function book_prices()
    {
        $book_price['min'] = Book::min('selling_price');
        $book_price['avg'] = Book::avg('selling_price');
        $book_price['max'] = Book::max('selling_price');

        return $book_price;
    }

}
