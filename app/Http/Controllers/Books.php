<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Book;

class Books extends Controller
{

    /**
     * Display all books
     * @param  integer $id Book ID
     */
    public function index($id = null) {
        if ($id == null) {
            return Book::orderBy('id', 'asc')
            ->where('books.deleted', '=', '0')
            ->join('authors', 'books.author_id', '=', 'authors.id')
            ->select('books.*', 'authors.name')
            ->get();
        } else {
            return $this->show($id);
        }//end if
    }//function index

    /**
     * Display the specified book
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function show($id) {
        return Book::join('authors', 'books.author_id', '=', 'authors.id')
                    ->select('books.*', 'authors.name as author_name')
                    ->find($id);
    }


    /**
     * Store a newly created resource in storage
     *
     * @return Response
     * @author Mohit
     */
    public function store(Request $request) {

        $this->validate($request, [
            'isbn' => 'required',
            'title' => 'required',
            'description' => 'required',
            'publish_date' => 'required|date_format:"m-d-Y"',
            'selling_price' => 'required',
            'cost_price' => 'required',
            'copies_in_stock' => 'required|numeric',
            'num_pages' => 'required|numeric',
            'author_id' => 'required|numeric',
            'publisher_id' => 'required|numeric',
            'category_id' => 'required|numeric',
            'image' => 'required',

    ]);

        $book = new Book;

        $book->isbn = filter_var($request->input('isbn'),FILTER_SANITIZE_STRING);
        $book->title = filter_var($request->input('title'),FILTER_SANITIZE_STRING);
        $book->description = filter_var($request->input('description'),FILTER_SANITIZE_STRING);
        $book->publish_date = filter_var($request->input('publish_date'),FILTER_SANITIZE_STRING);
        $book->selling_price = filter_var($request->input('selling_price'),FILTER_SANITIZE_STRING);
        $book->cost_price = filter_var($request->input('cost_price'),FILTER_SANITIZE_STRING);
        $book->copies_in_stock = filter_var($request->input('copies_in_stock'),FILTER_SANITIZE_STRING);
        $book->num_pages = filter_var($request->input('num_pages'),FILTER_SANITIZE_STRING);
        $book->author_id = filter_var($request->input('author_id'),FILTER_SANITIZE_STRING);
        $book->publisher_id = filter_var($request->input('publisher_id'),FILTER_SANITIZE_STRING);
        $book->category_id = filter_var($request->input('category_id'),FILTER_SANITIZE_STRING);
        $book->image = filter_var($request->input('image'),FILTER_SANITIZE_STRING);
        $book->save();

        return $book;
    }//end function store

    /**
     * Update the specified resource in storage
     *
     * @param int $id
     * @return Response
     * @author Mohit
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'isbn' => 'required',
            'title' => 'required',
            'description' => 'required',
            'publish_date' => 'required|date_format:"m-d-Y"',
            'selling_price' => 'required',
            'cost_price' => 'required',
            'copies_in_stock' => 'required|numeric',
            'num_pages' => 'required|numeric',
            'author_id' => 'required|numeric',
            'publisher_id' => 'required|numeric',
            'category_id' => 'required|numeric',
            'image' => 'required',

    ]);

        $book = Book::find($id);
        $book->isbn = filter_var($request->input('isbn'),FILTER_SANITIZE_STRING);
        $book->title = filter_var($request->input('title'),FILTER_SANITIZE_STRING);
        $book->description = filter_var($request->input('description'),FILTER_SANITIZE_STRING);
        $book->publish_date = filter_var($request->input('publish_date'),FILTER_SANITIZE_STRING);
        $book->selling_price = filter_var($request->input('selling_price'),FILTER_SANITIZE_STRING);
        $book->cost_price = filter_var($request->input('cost_price'),FILTER_SANITIZE_STRING);
        $book->copies_in_stock = filter_var($request->input('copies_in_stock'),FILTER_SANITIZE_STRING);
        $book->num_pages = filter_var($request->input('num_pages'),FILTER_SANITIZE_STRING);
        $book->author_id = filter_var($request->input('author_id'),FILTER_SANITIZE_STRING);
        $book->publisher_id = filter_var($request->input('publisher_id'),FILTER_SANITIZE_STRING);
        $book->category_id = filter_var($request->input('category_id'),FILTER_SANITIZE_STRING);
        $book->image = filter_var($request->input('image'),FILTER_SANITIZE_STRING);
        if ($book->save()) {
            return back()->with('message', 'Book Updated');
        } else {
            return back()->with('message', 'Problem occured while updating book');
        } //end if
    }//end function update

    /**
     * Remove the specified resources from storage
     *
     * @param int $id
     * @return Response
     * @author Mohit
     */
    public function destroy(Request $request, $id)
    {
        $book = Book::find($id);
        $book->deleted = 1;
        $book->save();

        return 'Book successfully deleted with id #' . $request->input('id');
    }//end function destroy
}//end class
