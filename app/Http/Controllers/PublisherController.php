<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Publisher;

class PublisherController extends Controller
{

    /**
     * Return all publishers
     * @param  int $id
     * @return Response
     * @author: Nigel
     */
    public function index($id = null) {
        if ($id == null) {
            return Publisher::orderBy('id', 'asc')
            ->where('deleted', '=', '0')
            ->get();
        } else {
            return $this->show($id);
        }//end if
    }//function index

    /**
     * Display the specified publisher
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function show($id) {
        return Publisher::find($id);
    }

    /**
     * Store new publisher
     * @param Request $request
     * @return response
     * @author Nigel
     */
    public function store(Request $request)
    {
        $publisher = new Publisher;

        $publisher->name = $request->input('name');
        $publisher->country = $request->input('country');
        $publisher->phone = $request->input('phone');
        $publisher->email = $request->input('email');
        $publisher->website = $request->input('website');
        $publisher->deleted = 0;
        $publisher->save();

        return 'Publisher record created with id ' . $publisher->id;
    }

    /**
     * Update existing publisher
     * @param Request $request
     * @return response
     * @author Nigel
     */
    public function update(Request $request, $id)
    {
        $publisher = Publisher::find($id);

        $publisher->name = $request->input('name');
        $publisher->country = $request->input('country');
        $publisher->phone = $request->input('phone');
        $publisher->email = $request->input('email');
        $publisher->website = $request->input('website');
        $publisher->deleted = 0;
        $publisher->save();

        return 'Publisher record updated with id ' . $publisher->id;
    }

    /**
     * Remove the specified publisher
     * @param int $id
     * @return Response
     * @author Nigel
     */
    public function destroy(Request $request, $id)
    {
        $publisher = Publisher::find($id);
        $publisher->deleted = 1;
        $publisher->save();

        return 'Publisher successfully deleted with id #' . $request->input('id');
    }

    public function publisher_detail($id, Request $request){

        $publishers = Publisher::find($id);
        $title = "Publisher Detail";
        return view('pages/publisher_detail',compact('title','publishers'));
    }

}
