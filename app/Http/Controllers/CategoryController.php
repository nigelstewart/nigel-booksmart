<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
    * Return all the data in category table
    *
    * @param type $id
    * @return response
    * @author Mohit Salhotra
    */
   public function index($id = null) {
       if ($id == null) {
           return Category::orderBy('id', 'asc')
           ->where('deleted', '=', '0')
           ->get();
       } else {
           return $this->show($id);
       }//end if
   }//function index

   /**
    * Display the specified category
    * @param int $id
    * @return Response
    * @author Nigel
    */
   public function show($id) {
       return Category::find($id);
   }

   /**
    * Store a newly created resource in storage
    *
    * @return Response
    * @author Mohit Salhotra
    */
   public function store(Request $request) {

       $this->validate($request, [
           'name' => 'required',
   ]);

       $category = new Category;

       $category->name = filter_var($request->input('name'),FILTER_SANITIZE_STRING);
       $category->deleted = 0;
       $category->save();

       return $category;
   }//end function store

   /**
    * Update existing category
    * @param Request $request
    * @return response
    * @author Nigel
    */
   public function update(Request $request, $id)
   {
       $category = Category::find($id);

       $category->name = $request->input('name');
       $category->deleted = 0;
       $category->save();

       return 'Category record updated with id ' . $category->id;
   }

   /**
    * Remove the specified category
    * @param int $id
    * @return Response
    * @author Nigel
    */
   public function destroy(Request $request, $id)
   {
       $category = Category::find($id);
       $category->deleted = 1;
       $category->save();

       return 'Category successfully deleted with id #' . $category->id;
   }
}
