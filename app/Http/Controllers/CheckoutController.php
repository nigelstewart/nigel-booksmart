<?php
/**
 * Description of CheckoutController
 * Controller for the checkout
 * @author Bhumi Patel <bpatel.7108@gmail.com>
 * @created: 2016-10-12
 * @updated: 2016-10-12
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Cart;
use App\Transaction;
use App\_5bx;
use App\Category;

class CheckoutController extends Controller
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->middleware('checkout');
    }

    /**
     * Checkout Page
     * @return mixed view
     */
    public function checkout()
    {
        $categories = Category::Orderby('id', 'asc')->get();
        $title = "Books Mart - Checkout";
        return view('shoppingcart.checkout', compact('title', 'categories'));
    }

    /**
     * checks the form inputs and sanitizes the input and stored in the database
     * @param Request $request
     * @return redirect or an error
     */
    public function store(Request $request) {
        $this->validate($request, [
            'shippingaddress' => 'required|max:255',
            'shippingcountry' => 'required',
            'shippingprovince' => 'required',
            'shippingcountry' => 'required',
            'ship_postcode' => 'required',
            'ship_first_name' => 'required',
            'ship_last_name' => 'required',
            'billingaddress' => 'required|max:255',
            'billingcountry' => 'required',
            'billingprovince' => 'required',
            'billingcountry' => 'required',
            'bill_postcode' => 'required',
            'bill_first_name' => 'required',
            'bill_last_name' => 'required',
        ]);
        $order = new Order;
        $order->shipping_first_name = filter_var($request->input('ship_first_name'), FILTER_SANITIZE_STRING);
        $order->shipping_last_name = filter_var($request->input('ship_last_name'), FILTER_SANITIZE_STRING);
        $order->shipping_address = filter_var($request->input('shippingaddress'), FILTER_SANITIZE_STRING);
        $order->shipping_country = filter_var($request->input('shippingcountry'), FILTER_SANITIZE_STRING);
        $order->shipping_city = filter_var($request->input('shippingcity'), FILTER_SANITIZE_STRING);
        $order->shipping_province = filter_var($request->input('shippingprovince'), FILTER_SANITIZE_STRING);
        $order->shipping_postal_code = filter_var($request->input('ship_postcode'), FILTER_SANITIZE_STRING);
        $order->billing_first_name = filter_var($request->input('bill_first_name'), FILTER_SANITIZE_STRING);
        $order->billing_last_name = filter_var($request->input('bill_last_name'), FILTER_SANITIZE_STRING);
        $order->billing_address = filter_var($request->input('billingaddress'), FILTER_SANITIZE_STRING);
        $order->billing_country = filter_var($request->input('billingcountry'), FILTER_SANITIZE_STRING);
        $order->billing_city = filter_var($request->input('billingcity'), FILTER_SANITIZE_STRING);
        $order->billing_province = filter_var($request->input('billingprovince'), FILTER_SANITIZE_STRING);
        $order->billing_postal_code = filter_var($request->input('billingpostcode'), FILTER_SANITIZE_STRING);
        $order->total = Cart::total() +  number_format(Cart::subtotal() * 0.08) + number_format(Cart::subtotal() * 0.05);
        $order->subtotal = Cart::subtotal();
        $order->gst = number_format(Cart::subtotal() * 0.08);
        $order->pst = number_format(Cart::subtotal() * 0.05);
        $order->order_date = date('Y-m-d h:i:s');
        $order->user_id = Auth::user()->id;

        if ($order->save()) {
            return redirect('/');
        } else {
            return 'There was a problem saving the post';
        }
    }

    /**
     * Processes the users payment
     * @param  Request $request Cake PHP > Laravel
     * @return mixed
     */
    public function process_payment(Request $request) {
        try {

            $transaction = new _5bx(env('_5BX_LOGIN_ID'), env('_5BX_API_KEY'));
            $transaction->amount(Cart::total());
            $transaction->card_num($request->input('cardNumber')); // credit card number
            $transaction->exp_date($request->input('expiryDate')); // expiry date month and year
            $transaction->cvv($request->input('cvvCode')); // card cvv number
            $transaction->ref_num('2011099'); // your reference or invoice number
            $transaction->card_type($request->input('card_type')); // card type
            $response = $transaction->authorize_and_capture(); // returns JSON object
        } catch (Exception $e) {

            die($e->getMessage()); // don't really die... do something useful
        }
        if ($response->transaction_response->response_code == 1) {
            $transaction = new Transaction;
            $encoded_object = json_encode($response->transaction_response);
            $transaction->transaction_status = filter_var($encoded_object, FILTER_SANITIZE_STRING);
            if ($transaction->save()) {
                $get_transaction = Transaction::get()->last();
                $id = $get_transaction->id;
               return redirect("thankyou/$id");
            } else {
               return 'Could not save your transaction details';
            }
        } else {
            $errors = get_object_vars($response->transaction_response->errors);
            $error_value = '';
            foreach ($errors as $key) {
                $error_value .= $key . '.';
            }

            return redirect('/payment/fail')->with('message', $error_value);
        }
    }


}
