<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use User;

class AdminUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user = Auth::user();
            if (count($user->admin())) {
                return $next($request);
            }
        } else {
            return redirect('/');
        }
    }
}
