<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
/*author: Bhumi*/
class Checkout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*check for logged in */
        if(Auth::check()){
            return $next($request);
        }
        return redirect('/login')->with('message','You must be logged in to checkout');
    }
}
