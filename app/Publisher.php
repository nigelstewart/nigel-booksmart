<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    /**
     * Created By: Sanders
     * returns the books the publisher has written
     * @return mixed
     */
    public function publisherBooks()
    {
        return $this->hasMany('App\Book')
                    ->join('publishers', 'publishers.id', '=', 'books.publisher_id')
                    ->select('books.*', 'publishers.*')
                    ->get();
    }
}
